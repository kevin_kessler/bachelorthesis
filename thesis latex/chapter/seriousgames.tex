\chapter{Serious Games}
\label{c:seriousgames}

Games have accompanied mankind since the dawn of time.
They are expressions of play, which is a basic part of the human nature.
Over the time a variety of different game types like sports games, board games or guessing games has evolved.
In this paper, however, the term \textit{game} refers to video games.

Video games (or digital games) are games that are controlled by a computer and usually serve to entertain the player. 
But not all games are made for pure entertainment purposes. 
The aforementioned game applications from section \ref{sec:medialab} can be taken as an example.
These games teach the player about science while they interact with them. 
Accordingly, their main goal is to inform about physics rather than to entertain the player.
This is what positions them within the \textit{Serious Game} paradigm.

\section{What is a Serious Game}
\subsection{Definition}
\label{subs:seriousgame_definition}
When reading about serious games, one finds different definitions about what a serious game actually is. 
Many of those are often based on what the person giving the definition does.
In other words, they often differ depending on the context and on the person who determines them.
For example, the definition given by the game designers David Michael and Sande Chen is too specific: 
\begin{quote} 
\centering 
``A serious game is a game in which education (in its various forms) is the primary goal, rather than entertainment.''\cite{Michael:2005:SGG:1051239}
\end{quote}

\noindent
This definition expects always an educational purpose from a serious game. However, it is easy to find an example which does not match this requirement.
For instance, a game which tries to improve the health of its players' by encouraging them to move, does not necessarily have anything to do with education.
Nevertheless, it still can be considered as serious. Thus, a more general definition for the term is required.

Ben Sawyer, a pioneer and co-founder of important serious game initiatives, defines them as follows:
\begin{quote} 
\centering 
``Any meaningful use of computerized game/game industry resources whose chief mission is not entertainment.''\cite{sg_landscape}
\end{quote}

\noindent
According to his definition serious games spread to far more areas than only education.
Consequently he disagrees with the game designers' opinion in that point. 
But more important is the aspect, which both definitions have in common.
They agree that, contrary to usual video games, serious games are designed for a primary purpose other than entertainment. 
Furthermore, this is the core statement, which most definitions for serious games coincide with.
No matter if designed for areas like defence, politics, health care or education, a serious game can always be denoted as a game for non-entertainment purposes.

The term \textit{Serious Game} itself was coined by Sawyer in 2002 in his paper about the usage of digital games for public policy (see \cite{sawyer_gamebasedlearning}).
By coining the term and co-founding the \textit{Serious Games Initiative}\footnote{\url{http://www.seriousgames.org/} [January 2014].} as well as the \textit{Games for Health Project}\footnote{\url{http://www.gamesforhealth.org/} [January 2014].}, Sawyer became a recognized leader within the serious games community of the United States of America.


\subsection{Purposes and Classification}
After reading that serious games are not primarily aiming at entertainment, the question of what their main purpose is suggests itself.
The answer is: it depends.
For instance, some of them are intended to teach or train the player in a specific subject, whereas others can be foreseen to improve the health of their players or to spread propaganda. 
Therefore a classification depending on their purpose makes sense.
According to Sawyer and the games researcher Peter Smith, serious games can partially be classified by their purpose as follows (see also \cite{sg_taxonomy}):

\begin{itemize}
\item Work
\item Advertisement
\item Education
\item Health
\item Science and Research
\item Training
\item Production
\end{itemize}

\noindent
However, this approach is not specific enough to create a clear definition for the classification of serious games.
As an illustration, two of them which belong to the \textit{Games for Training} class, can be compared. 

In the game \textit{Zero Hour: America's Medic}\footnote{\url{http://serious.gameclassification.com/EN/games/17987-Zero-Hour-Americas-Medic/index.html} [January 2014].} 
the players can improve and apply their skills as a medical first responder. 
In a virtual environment which simulates a crash scene, the players have to save the lives of the injured e.g. by bandaging wounds. 
The game \textit{Disney Stars}\footnote{\url{http://serious.gameclassification.com/EN/games/43213-Disney-Stars-The-Virtual-Sell/index.html} [January 2014].} on the other hand,
provides the virtual setting of a travel agency and puts the player into a sales conversation with virtual customers. 
The player's objective is to sell a holiday trip to the client. 

As a result one can see that both of the introduced games pursue the purpose to train their players.
The target audiences, however, are totally different. 
While the first example addresses medics, the second one is only intended for salesmen.
For that reason we also have to specify the market at which a serious game is aimed.
Sawyer distinguishes between the following markets (see also \cite{sg_taxonomy}):

\begin{itemize}
\item Corporate
\item Defence
\item Education
\item Government and Non-Governmental Organisations
\item Healthcare
\item Industry
\item Marketing and Communications
\end{itemize}

\noindent
By combining the different purposes and markets, Sawyer and Smith created the \textit{Taxonomy of Serious Games}, which is shown in figure \ref{graphic:taxonomy}.


\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/Sawyer_SeriousGamesTaxonomy}
\caption{Serious Games Taxonomy by Ben Sawyer and Peter Smith (Source: \cite{sg_taxonomy})}
\label{graphic:taxonomy}
\end{figure}


The taxonomy is their attempt to create a clear definition of how to classify serious games. According to it the two previously mentioned games can be clearly classified. 
\textit{Zero Hour: America's Medic}, whose market category is \textit{Healthcare} can be considered as \textit{training game for health professionals}, 
whereas \textit{Disney Stars}, which belongs to the corporate market, falls into the category \textit{Employee Training}.


\subsection{The most Common Type}
\label{subs:most_common_genre}
To find out which category includes the largest amount of serious games, the sizes of the markets as well as the distribution of the purposes are investigated. 

At first the markets are explored.
According to Sawyer's presentation \cite{sg_taxonomy}, which he held at the \textit{Serious Games Summit 2008}, the education market was leading by that time.
To see if this is still valid, the database of the collaborative classification system \textit{serious.gameclassification.com} (henceforth referred as SGC), which contains roughly 3000 entries of serious games, is examined. 
The SGC is suited to video games and resulted out of an academic research project\footnote{\url{http://serious.gameclassification.com/EN/about/article.html} [January 2014].} in association with the \textit{Toulouse Universities II and III}.
By offering a set of filters, which are grouped by purpose and market, the service provides the possibility to search for specific games from various sectors.
For this investigation, the sizes of the respective markets are determined by the number of matching entries of the database.
After collecting the data, a pie chart which sets the collected numbers in relation, is created. The illustration of the result is shown in figure \ref{graphic:marketchart_sgc}.

%\includegraphics[width=\textwidth]{seriousgames/marketchart_gameclassification_com}{Distribution of serious games by market according to the database of serious.gameclassification.com (Jan 2014)}
%\label{imagescaled:marketchart_sgc}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/marketchart_gameclassification_com}
\caption{Distribution of serious games by market according to the database of serious.gameclassification.com (Jan. 2014)}
\label{graphic:marketchart_sgc}
\end{figure}


Although the SGC divides the market of serious games into more sections than Sawyer does, the result stays the same.
With a share of 42\%, the education sector remains the leading market for serious games.

In the next step, the distribution of purposes is examined.
Once again the SGC's database serves as the source for the investigation. 
The method for gathering data does not change but the search results are filtered by purpose instead of markets.
Figure \ref{graphic:purposechart_sgc} shows the resulting incidence of serious games within the respective field of purpose.

%\includegraphics[width=\textwidth]{seriousgames/purposechart_gameclassification_com}{Distribution of serious games by purpose according to the database of serious.gameclassification.com (Jan 2014)}
%\label{imagescaled:purposechart_sgc}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/purposechart_gameclassification_com}
\caption{Distribution of serious games by purpose according to the database of serious.gameclassification.com (Jan. 2014)}
\label{graphic:purposechart_sgc}
\end{figure}


With a share of 44\%, it becomes obvious that, according to the SGC's database, most of the serious games are made for educational purpose.

After finding the biggest market and the most frequently used intention, the most common type can be determined. 
By having a look at Sawyer's taxonomy (see figure \ref{graphic:taxonomy}) and applying the results, this type turns out to be \textit{Learning}.
That means that most of the existing serious games can be classified as belonging to this type.
Furthermore, this conclusion conforms to Sawyer's and Smith's projected taxonomy from 2008, which illustrates the distribution of serious games to the respective categories (see figure \ref{graphic:projected_taxonomy}). 
By showing that the biggest amount of (their observed) serious games belong to the combination of the educational market and the educational purpose (the learning genre), the result is confirmed once more.


%\includegraphics[width=\textwidth]{seriousgames/eludamos_whysoserious_educationalconcepts}{The relations between serious games and similar educational concepts INSERTHERE}
%\label{imagescaled:educationalconcepts_whysoserious}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/Sawyer_SeriousGamesProjectedTaxonomy}
\caption{Projected taxonomy of serious games by Ben Sawyer and Peter Smith (2008, Source: \cite{sg_taxonomy})}
\label{graphic:projected_taxonomy}
\end{figure}



\section{Benefits of Usage}

\subsection{Serious Games as Educational Concept}
The idea of using media for teaching and/or learning is not new. 
There are many established educational concepts with both, analogue and digital media.
A conventional concept is learning and teaching by analogue media like texts or books.
Modern ones include the usage of digital media like the Internet or educational films.
Serious games belong to these modern concepts.
Among them, they can be positioned as shown in figure \ref{graphic:sg_educoncepts} (see also \cite{educoncepts}).

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/educational_concepts}
\caption{The Position of Serious Games Among Other Educational Concepts (Source: \cite{educoncepts})}
\label{graphic:sg_educoncepts}
\end{figure}

\noindent
It shows that the biggest application field of serious games belongs to the entertaining education field, meaning educational concepts that try to make learning enjoyable. 
The small part which is not settled within this field, refers to serious games that are not aiming on education (e.g. games for health).
On the contrary, the DGBL (Digital Game Based Learning) segment represents those whose sole purpose is teaching and/or learning, meaning all serious games that belong to the most common type (see \ref{subs:most_common_genre}).
The main characteristic of DGBL is that knowledge/education is imparted automatically while interacting playfully with the game.

Although serious games are recognized as a modern educational concept, their application still needs to emerge and find greater acceptance.
Kindergartens, Schools, colleges, and other educational institutions could profit a lot from serious games, if they used them.
Therefore, this section explains why it makes sense to use serious games as a new teaching and learning concept.
Furthermore, general benefits of the usage of serious games are discussed.

\subsection{Games as Part of our Society}
\label{subs:sg_part_of_society}
Games attract people. This is the first reason that supports the idea of including serious games in modern learning concepts.
According to the reports \textit{Essential Facts About the Computer and Video Game Industry} from 2011 and 2013, by the Entertainment Software Association (ESA), 
72\% of U.S. American households and 58\% of all U.S. Americans play video games (\cite{ESA_EF11} and \cite{ESA_EF13}). 
The numbers for European countries are similar (see \cite{ISFE_Consumer_Study}). 
In 2012, Sweden was leading with a game-playing population of 62\%. The bottom placed country was Portugal with (still) 40\%.
The average percentage of players per country in Europe is a bit less than in the U.S., but still impressive: 48\%.
Especially for children, games seem to be a part of their every day life. 
As a survey found out (see \cite{pip_teens_videogames}), 97\% of children between the age of 12 and 17 play video games. Half of them do it even every day. 
Furthermore, the ESA shows that the games industry's revenues per year doubled, from \$7.3bn to \$14.8bn, during the last decade and even tripled since the year 2000 (\cite{ESA_EF11} and \cite{ESA_EF13}). 

Thus it seems obvious that games are an accepted and well integrated part of our society. 
Will Wright, a famous game designer, once said:
\begin{quote} "If a learner is motivated, there's no stopping him [or her]". \end{quote}
By regarding the above presented numbers, it can be said that people exhibit a high level of motivation while playing video games and spend hours in virtual worlds.
This motivation can and should be used for educative or informative purposes.
If players are motivated to achieve the goals of a serious game, they deal automatically and sometimes unknowingly intense, with the specific learning topic.

\subsection{Positive Impact}
As stated in the book \cite{conf/sgda/2012}, there is already a lot of evidence which proves that educational serious games can have a positive impact on learning outcomes.
Furthermore, fundamental abilities like reading, orientation or logical thinking, and advanced skills like reading maps, solving problems and teamwork can be developed while playing games.
Moreover, the collected in-game experience remains stored in the players' heads. This retention is longer lasting and more intense in comparison to other forms of teaching (see \cite{gespielte_ernst_des_lebens}).

In addition to these cognitive faculties, serious games can affect the players' physical condition in a positive way.
Serious games that improve the players health or sportiness are usually classified as \textit{exergames} (see figure \ref{graphic:taxonomy}). 
The famous exergame \textit{Dance Dance Revolution} (DDR) by KONAMI can be mentioned as an example. 
To play DDR players have to stand on a special game controller, which usually lies in form of a mat in front of a screen that runs the game. 
In order to follow the instructions given by the screen, the players have to 'dance' on that game controller.
As a result, the players' health and fitness can be improved by playing the game.

\subsection{Safety and Cost}
Other noteworthy aspects in coherence with training and education are high costs and safety issues.
The amount of money that some institutions pay for apprenticeships or training of employees and students can be very high. 
Medical tools, for example, can be very expensive. Rather than buying them, a serious game, which provides those tools virtually, could be used to train students in dealing with them.
In addition to that, the risks of hurting oneself, others or damaging the tools while working with them, disappears. This shows that safety risks and costs can be minimized by the usage of serious games.

Of course, especially in training games for healthcare, serious games must be designed in cooperation with the respective institutions and/or subject matter experts.
For instance, if medics are trained by a badly designed game, they can cause more harm in practice than they may be able to help. 

\subsection{Privacy and Feedback}
A common problem in courses at schools or universities is that the students often do not dare to participate actively.
The fear of making a mistake in front of others prevents them from giving answers or expressing their opinions. 
Consequently the students do not always earn feedback.
With a serious game, students can learn in privacy. They are encouraged to use and improve their skills or knowledge without being judged by anyone.
This way, students are able to prove their knowledge and to immediately retrieve feedback by the system.

\subsection{Didactic}
From the didactic point of view, there is a huge potential of using digital and interactive media, such as serious games, as a form of teaching and/or learning.
Michael Kerres, a German pioneer of E-Learning concepts, names the following important potentials of that usage (see \cite{mediendidaktik_kerres}): 

\begin{itemize}
\item Support of other learning concepts and new qualities of learning,
\item A different organisation of learning, and
\item Shortened learning periods.
\end{itemize}

\noindent
Projected to serious games, those potentials apply as follows.

\subsubsection{Support of other learning concepts and new qualities of learning}
By a serious game's media elements like images, videos or animations, certain learning contexts can be perfectly illustrated.
This illustration can be used to support other teaching approaches such as courses at an university.
Furthermore, learners can project their thoughts cognitively and emotionally into the learning tasks of a serious game.
Thus they establish a good understanding and retention of the solved tasks.

\subsubsection{A different organisation of learning}
Serious games give more flexibility in the organisation of teaching and learning.
In the contrary to conventional learning concepts like lectures at an university, the learners are not necessarily bound to fix learning periods or learning places, when playing serious games.
Serious games can be played at home and at any time, and if they are well designed, they do not need a teacher nearby to explain the addressed topics.
Thus, these games become to a useful complement for teaching and learning concepts.
Especially students in correspondence courses or distance learning courses can profit from the usage of serious games, as those are available at any time and any place and illustrate the topics in a vivid way.

\subsubsection{Shortened learning periods}
In school classes, for example, the teacher determines the pace of teaching.
On the one hand, this can slow down some students' progress, while on the other hand some students may be lost by the teacher.
Players of serious games, however, can determine and adjust the way they play/learn.
This means that they can regulate their pace of learning and, if required, repeat specific tasks to deepen their understanding.
Thus, shortened learning periods (in average of the group) can be reached.

\section{History}
Games are as old as mankind. But does that mean that games with a serious purpose already existed back in time?
This section covers how serious games evolved from the early beginnings up to today.

\subsection{Pre-Electronic Era}
Even tough the term \textit{serious game} is quite young in the modern sense (see subsection \ref{subs:seriousgame_definition}), games with a primary goal other than entertainment were already used thousands of years ago. 
From fortune telling with the bones of animals up to teaching of economics, games were used for many purposes, which were considered as serious in their respective times.

The usage of strategy board games, whose development was promoted mostly by the military sector, can be taken as an example.
These games were primarily used for planning military manoeuvres, such as the capturing of enemy territories, rather than for fun. 
It was, for example, in the early 19th century, when a lieutenant of the Royal Prussian Army invented a game called \textit{Kriegsspiel} (the German word for war game). 
By providing a special table, different terrain types, dice and porcelain soldiers, the lieutenant was able to simulate a war scene. By playing this game, officers of the Prussian army were educated in warfare. 

 
\subsection{Computer Era}
\label{subs:sg_computer_era}
Shortly after the advent of the computer age, the idea of digital games emerged.
\textit{Spacewar!}, a science-fiction game which was conceived at the MIT in 1961, is known as one of the first digital games.
Nevertheless it took roughly 20 years to discover the potential of digital games at serving for serious purposes. 
It was in 1980 when the Atari Games Corporation designed the game \textit{Army Battlezone}, which is considered to be one of the first digital serious games. 
As its  name implies, it was used for military training. This example shows that the military sector promoted the development of serious games again. 
Therefore it is not surprising that the most famous serious game, called \textit{America's Army}, has been developed and published by the United States Army.
It has been released in 2002 and was used to help the army with the recruitment of soldiers.
Furthermore, it was distributed for free and found great acceptance among the public.

Besides the release of \textit{America's Army}, 2002 counts as an important year for serious games. 
Since Ben Sawyer coined the term \textit{serious game} during that year (see also subsection \ref{subs:seriousgame_definition}), it is usually considered as the advent of the current serious games wave.
Some people even distinguish between serious games that have been released before 2002 and the ones that came after.
To investigate the growth of the wave, the entries of the SGC's database are examined once again (see also subsection \ref{subs:most_common_genre}).
After clustering them by their year of release, the collected data is plotted as shown in figure \ref{graphic:timegraph_sg}.

%\clearpage

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{seriousgames/timegraph_sg}
\caption{Growth of serious games since 1980, according to the database of serious.gameclassification.com (Jan. 2014)}
\label{graphic:timegraph_sg}
\end{figure}

%\noindent
It clearly shows that the number of serious games has more than doubled during the last decade.
In addition to that, a particular striking increase of the growth rate around 2007 can be spotted.
To investigate this in detail, the growth rates from 2005 to 2006 and from 2006 to 2007 are calculated by using the formula 
\begin{math}
rate = \frac{n(T_2)-n(T_1)}{n(T_1)}.
\end{math}
The result shows that the growth rate has doubled from $\sim$7\% to $\sim$14\% within only one year.
This is most likely due to the increased appearance of modern mobile (touch screen) devices in 2007.
Additionally it is possible that the release of new game engines made an impact on the growth of the last decade.
The free availability and the powerful tools of certain modern game engines, allow even independent developers to create games.
Consequently, a variety of serious games were contributed by them.
