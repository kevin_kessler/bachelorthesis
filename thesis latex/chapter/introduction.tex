\chapter{Introduction}
\label{c:introduction}
%\printinunitsof{cm}\prntlen{\textwidth}
Games are as old as mankind. 
Playing them is in the nature of man.
It is a special form of interaction and communication that has evolved over time.

Nowadays, the term \textit{game} is more and more associated with digital or video games.
During the past two decades, the video game industry grew to a giant economic sector that attracts and involves a countless number of people such as players and developers.
Furthermore, it comprises a various amount of different game genres like first person shooters, role play games or puzzle games.

With the growth of the game industry, the demand for the fast delivery of new video games increases steadily.
Thus, modern tools that support the development of games are needed.
Usually, special software frameworks called \textit{Game Engines} are used to accelerate the process of video game development. 
There are many different game engines from which each has its pros and cons for the creation of certain game types.

For a long time, video games were mainly produced to entertain players in order to make financial profit.
Over the past twelve years, however, a wave of games that are used for purposes like education, information, or training have emerged.
Those games are known as \textit{Serious Games} because they are used in a more serious context than the entertainment ones.
%evtl gut für intro http://infinitewrench.wordpress.com/2012/09/07/unity3d-vs-unreal-development-kit/

There are various fields of application for such games.
Besides private usage, serious games are applied to many different industries like defence, healthcare, or education.
Institutions from the healthcare sector, for example, use them for rehabilitation of their patients or to train their staff.
%Games from the defence sector are usually used to train soldiers or members of the police.
Educational and research institutions, such as universities or CERN, use serious games to explain and teach difficult subjects in a compelling way.

\section{Motivation}
Not only the demand for entertainment games is increasing.
The society's interest in serious games is growing as well.

Especially at institutions like CERN, which are visited by several hundreds of people per day, the demand for presenting knowledge and information in interesting and illustrative ways is vast.
In order to raise the visitors' interest and to ultimately enhance their knowledge, it is no longer enough to create flyers and posters that explain a complex subject.
Modern media opens new possibilities and makes former barriers disappear.

The big advantage of combining a serious purpose with the fun characteristics of games is the high level of motivation that players experience while playing them.
A motivated player who is willing to achieve the goals of a serious game is automatically and mostly unknowingly exposed to its serious intention.
Thus, players are encouraged to interact playfully with topics that might be inconvenient or hard to understand when dealing with it in another way.
Of course, in order for this to be effective and intense, the serious game itself and its subject specific content must be designed very well.

For these reasons, the usage and production of high-quality serious games is becoming more and more important.
Like any other valuable software development process, the development of a serious game requires suitable tools and needs to be well organised.
There is a variety of existing game engines for modern game development but none of them is specifically designed for the development of serious games.
However, this does not exclude that a serious game can be designed with one of those. 
Many serious games have already been implemented by using them.
It is rather a question of the game engine's suitability for serious games development.

\section{Goals}
\label{s:intro_goals}
As mentioned before, there is a variety of existing modern game engines.
To successfully develop a high-quality serious game, it is important to use a game engine that suits the needs of general serious games development.

Thus, it is subject of this paper to determine which of the modern game engines is the most suitable for this kind of development.
Therefore, this paper needs to provide a better understanding of the terms \textit{Serious Game} and \textit{Game Engine}.
Moreover, modern game engines must be introduced and investigated.
A paper based, systematic comparison of those should ascertain the, in theory, most suitable game engine.
Furthermore, the results of the development of a serious game with the determined engine should either prove or refute the theoretical assumption.
In order to develop it, this paper also needs to introduce a development process approach.

In the end, this paper is meant to serve as a case study for those who are struggling with the choice of a suitable game engine for the realisation of their serious game idea.
Additionally, it should impart the knowledge about how to develop a serious game with the chosen engine by developing the prototype and explaining the important aspects of the serious games development process.

\section{Structure of the Paper}
To give an overview of the methodology and the structure of the whole paper, the content of each chapter will be outlined briefly in the following.

While the current chapter explains the motivation and purpose of this paper, chapter \ref{c:cernedu} introduces CERN, which served as the environment for writing the paper.
Furthermore, it describes how CERN tries to accomplish its educational mission and ends with the introduction of two of CERN's serious games that are used to support this mission.

In chapter \ref{c:seriousgames}, the term \textit{Serious Game} is explained in detail. 
Additionally, the different purposes and fields of application of serious games are listed and used to introduce a serious games classification system.
Afterwards, a database about serious games is examined in order to determine the most common type, which is needed for reference in other chapters.
Then, the benefits of using serious games are explained and a short outline of their history from the early stages up to today is given.

Chapter \ref{c:gameengines} serves to provide a better understanding of game engines, their components, and their different types.
This information is needed in order to define the comparison aspects for the systematic comparison in a later chapter.
Furthermore, a variety of leading modern game engines is introduced and compared superficially in order to narrow down the choice of the most suitable game engine for serious games development.
After sorting some of them out, two game engines remain to be compared in detail.

The systematic comparison of the two engines chosen in chapter \ref{c:gameengines} is provided by chapter \ref{c:comparison}.
Therefore, a set of comparison aspects is defined from which each serves as subject of investigation for both of the engines.
This means that the respective engine's capability in respect to the individual aspects is examined and rated independently.
At the end of the comparison, the results are summarized in order to determine the most suitable game engine.

Since the systematic comparison of chapter \ref{c:comparison} is done on a paper basis, its theoretical result is verified by chapter \ref{c:development} in form of the development of a serious game's prototype.
In the course of it, an approach for a serious games development process with the chosen game engine is introduced and applied.
At the end of the chapter, the result of the evaluation of the development process shows if the chosen game engine is also suitable for practical application.

Finally, in chapter \ref{c:conclusions} the results of this paper are summarized and evaluated and a short prospect to potential future work is given.

\section{Allocation of Project Report and Bachelor's Thesis}
This paper comprises the project report about the author's practical project during his stay at CERN and his bachelor's thesis.
The following subsections show which parts of this paper belong to the project report and which parts belong to the thesis\footnote{Required because of formal reasons.}.

\subsection{Project Report}
The main part of the practical project was the development of an interactive multimedia presentation software for internal use at CERN.
The main tool that was used for this development is called \textit{Unity}. 
It is one of the modern game engines that are subject of this paper.
Thus, the information about CERN, most of the Unity related sections and the appendix about the presentation software belong to the project report.
The following listing provides a clear attribution:
\begin{itemize}
\item Chapter \ref{c:cernedu}, which introduces CERN, its Education Group, and its Media Lab
\item Section \ref{subs:ge_unity4}, which introduces Unity
\item All the sections that are used to rate the comparison aspects of Unity in chapter \ref{c:comparison}. 
      This includes the subsections about Unity contained in \ref{s:comparisonaspect_development}, \ref{s:comparisonaspect_functionalities}, \ref{s:comparisonaspect_compatibility}, \ref{s:comparisonaspect_guicreation},  and \ref{s:comparisonaspect_support}.
\item Appendix \ref{appendix:impress}, which presents the aforementioned presentation software
\end{itemize}

\subsection{Bachelor's Thesis}
The remaining parts belong to the bachelor's thesis.
These are:
\begin{itemize}
\item Explanation of serious games in chapter \ref{c:seriousgames}
\item Explanation, introduction, and limitation of the choice of game engines in chapter \ref{c:gameengines}
\item The systematic comparison in chapter \ref{c:comparison}
\item The development of a serious game in chapter \ref{c:development}
\end{itemize}


%glossary http://en.wikibooks.org/wiki/LaTeX/Glossary 
%oder nur acronym verzeichnis und wichtige per fußnote erklären?
%SDK
%FPS
%indie
%NPC
%UE
%UDK
%SGC
%GUI
%WYSIWYG
%SME subject matter expert
%UML
%DGBL
%XML (Extensible Markup Language)


