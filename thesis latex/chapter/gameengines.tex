\chapter{Game Engines}
\label{c:gameengines}

\textit{CryENGINE}, \textit{Frostbite Engine}, or \textit{Unity} are terms that are often used in articles or news about video games.
They are the names of certain game engines, which are used to create video games. 
Although the term \textit{game engine} is widely used, persons without further knowledge about the game development field, usually do not know what it really means.
This chapter provides a better understanding about game engines and gives an overview over the variety of the existing ones.
Furthermore, the two game engines, which serve for the comparison in chapter \ref{c:comparison}, are selected and introduced.

\section{What is a Game Engine}
\label{section:what_is_a_game_engine}
Nowadays games can be incredibly realistic. 
Their visual appearance as well as their simulation of physical events improve steadily.
These and many other functionalities, such as artificial intelligence or audio playback, are managed and controlled by the game engine of the respective game.
Simply said, the game engine is what makes the game go.

Observed in detail, however, a game engine is more than that.
It is a software package which consists of a set of reusable, game-related components (see figure \ref{graphic:engine_components}).
Similar to the engine of a car, it is rather the components that do the actual work.
The game engine itself is just the container that combines them to a framework or even to a development environment.
The purpose of this combination is to ease and accelerate the game development process.
By bringing those components together, the game engine provides an abstraction of their functionalities.
Thus, the developers can focus on the details of their game instead of wasting time by programming rendering functions or doing physical calculations.
The individual components of the engine take care of these things.
A brief explanation of the most common components of a typical game engine and the different types of game engines is given by the following subsections.


\subsection{Components}
Just like a car is driven by its engine, a video game is powered by a game engine.
In both cases, it is the combination of the components that make the engine to a powerful tool.
The most common components of a typical game engine are listed in figure \ref{graphic:engine_components}.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{gameengines/engine_components}
\caption[]{Typical components of a game engine\footnotemark}
\label{graphic:engine_components}
\end{figure}
\footnotetext{Source: \url{http://gamyguru.wordpress.com/2012/07/16/inside-the-game-general-game-structure/} [January 2014].}

\noindent
In the following an explanation for each of the typical components is given.

\subsubsection{Graphics Engine}
The graphics engine (or graphics library), which is responsible for the visual representation of the game, is probably the most important component of a game engine.
It usually provides a huge amount of graphical functionalities, which game developers would have to program all by themselves if the engine did not have this part.
Visual effects like shadows, reflections or explosions are usually handled by it. 
Moreover it determines the geometrical object descriptions, manages the loading and application of textures and much more.
By rendering the data of the game scene, the graphics engine produces the visual representation of this data from a certain perspective.
Especially video games that play in a 3D environment benefit from a comprehensive and modern graphics engine (3D engine). 
This is partially because their commercial success often depends on the quality of their visual representation. 
Besides the game sector, graphics engines are also used in other fields like digital movies or modelling tools.
Examples for graphics engines (from low level abstraction to high level abstraction) are: 
\textit{OpenGL}\footnote{\url{http://www.opengl.org/} [January 2014].}, 
\textit{Direct3D}, 
\textit{Irrlicht Engine}\footnote{\url{http://irrlicht.sourceforge.net/} [January 2014].}, and 
\textit{OGRE}\footnote{\url{http://www.ogre3d.org/} [January 2014].}.

\subsubsection{AI Engine}
The engine that gives life and a natural behaviour to the non-player characters (NPCs) of a game, is called AI engine.
AI stands for \textit{artificial intelligence} and describes the intelligence expressed by software or machines.
Those engines provide functionalities that allow NPCs to find their way through the virtual world of a video game.
In addition, they determine the actions and decisions of a NPC in respect to the current situation in the game.
In first person shooters, for example, the AI causes NPCs to take cover when they are under attack.
As an example for a AI engine, the AI system of the \textit{Unreal} game engine\footnote{\url{http://www.unrealengine.com/en/features/artificial\_intelligence/} [January 2014].} can be taken.


\subsubsection{Physics Engine}
To provide highly realistic interactions with the game environment, modern game engines include a physics engine.
It is used to simulate the laws of physics (mainly gravity) within the virtual world. 
Furthermore, it determines the behaviour (dynamics) of rigid bodies, soft bodies and fluids.
Rigid bodies usually represent firm objects (shapes) such as cubes or cylinders, whereas soft bodies are deformable objects like cloths or ropes.

A common feature provided by the rigid body dynamics is called \textit{collision detection}.
As its name implies, it allows two objects to physically interact with each other, for example, like a ball that bounces on the ground.
Furthermore, rigid body dynamics can also be used to create the so called \textit{ragdoll physics}, which simulate realistic character movements by combining several rigid bodies with joints.
Soft body dynamics, on the other hand, take care of the realistic deformation of objects like a curtain that is waving in the wind.
To simulate realistic behaviour of water, blood or other liquids, a physics engine provides the fluid dynamics.

Some of the commonly used physics engines are: 
\textit{Open Dynamics Engine}\footnote{\url{http://www.ode.org/} [January 2014].}, 
\textit{Bullet}\footnote{\url{http://bulletphysics.org/wordpress/} [January 2014].},
\textit{PhysX}\footnote{\url{https://developer.nvidia.com/physx} [January 2014].}, and
\textit{Havok Physics}\footnote{\url{http://www.havok.com/products/physics} [January 2014].}.


\subsubsection{Sound Engine}
Another important part that lets a game appeal more impressive, is the sound engine (or sound system).
It usually provides functionalities for the playback and spatial positioning of music and sound effects.
To give players the impression of being in the middle of the action, modern sound engines support surround sound technologies like 5.1 or 7.1.
Additionally, some sound engines are able to transform sounds in such a way that they fit to the current virtual environment of a video game. 
Voices, for example, can be adapted to sound like they are used in a reverberating area or just in a normal room, without the need to record them twice.

Widely used sound engines are the \textit{Environmental Audio Extensions}\footnote{\url{http://www.creative.com/oem/technology/eax.asp} [January 2014].} and the \textit{Miles Sound System}\footnote{\url{http://www.radgametools.com/miles.htm} [January 2014].}.


\subsubsection{User Control System}
The user control system (or input system) handles the input that is given by the player in order to control certain elements of a game like a character, the camera or the navigation menu.
Furthermore, it determines which input devices can be used with the game engine.
There are many different types of input devices such as keyboards, mice, different types of gamepads, touch screens or gesture recognition devices.
The amount of supported types usually depends on the platforms to which the game engine can deploy its games.
If, for example, the game engine can deploy games for consoles and mobile devices, the input system should at least support gamepads and touch screens.


\subsection{Types}
There are different opinions of how game engines can be distinguished.
An often used approach is to categorize them by their graphics engine or rather by the type of its used dimension (2D, 3D).
Another way is to distinguish them by their supported platforms for deployment (PC, Console, Mobile).
But since there are game engines that support more than one type of dimension and/or more than one type of platform (see section \ref{section:ge_variety}), these approaches are not sufficient to clearly distinguish them.

To find a better one, the aforementioned analogy of the car engine can be used again.
A weak car-engine differentiates from a strong one by the arrangement, the combination, and the amount of its components.
Accordingly it is useful to classify game engines in a similar way. 
As explained at the beginning of this section (see \ref{section:what_is_a_game_engine}), a game engine provides an abstraction of its component's functionalities.
Thus, the combination and the amount of a game engine's components affect the level of its abstraction. 
That is why for their classification the following categories have been chosen\footnote{See also: \url{http://www.gamecareerguide.com/features/529/what_is_a_game.php?page=2} [January 2014].}:

\begin{itemize}
\item Low-Level Abstraction
\item Mid-Level Abstraction
\item High-Level Abstraction
\end{itemize} 

\subsubsection{Low-Level Abstraction}
Game engines that do not provide enough components to create a full game out of the box belong to this category.
Because of their lack of several important parts like the sound engine and the physics engine, developers have to make a lot of effort to implement the functionalities of those components.
On the other hand, however, this can also be an advantage, as it allows the developers to choose the most suitable tools for their game.
Furthermore, the chosen components can be integrated into the game engine and do not need to be developed from scratch.
Nevertheless a lot of additional work can be caused because of appearing compatibility issues towards the other components, which then have to be resolved.

In conclusion, it can be said that these engines can indeed give some freedom in the choice of components, but this is only at the expense of the duration and quality of the development process.
%example: allegaro http://alleg.sourceforge.net/news.html

\subsubsection{Mid-Level Abstraction}
Some game engines are especially designed for a specific type of game. 
Therefore, they only include components that are relevant for this particular type.
To develop a quiz game, for example, a game engine does not necessarily need to contain a physics engine.
The outcome would still be a complete game. 
However, the same engine could not be used to create a football game, because it would require the features from a physics engine.
To use the game engine nevertheless, the missing component would need to be integrated, what again causes a slowed-down development process. 
On the other hand, however, it again gives some freedom in the choice of tools.

To sum up: mid-level game engines, can be used to develop specific games, but require additional work if features of missing components are needed.


\subsubsection{High-Level Abstraction}
Game engines that belong to this category usually bring all relevant components that are needed to create a complete game.
Furthermore, they provide a high abstraction of their components' functionalities, e.g. in form of a integrated development environment.
Thus, complex tasks like creating 3D objects and assigning physical characteristics to them, can, for example, just be done by drag and drop.
This can accelerate a game development process enormously.
But there is also a problem that comes with those engines.
By imposing to use exactly the components that they provide, they limit the free choice of tools.

All in all those game engines are the best tools to create games in a short period of time, but also may limit the possibilities for the developers.


\section{Variety}
\label{section:ge_variety}
The aforementioned growth of the games industry during the past decade (see subsections \ref{subs:sg_part_of_society} and \ref{subs:sg_computer_era}), 
did not only bring up a countless amount of video games, but also a wide range of game engines, which served as tools for their development.
This section gives an overview of the leading\footnote{Leading according to \url{http://www.moddb.com/engines/top} [January 2014].} high-level abstractive game engines, their licensing programmes, their target platforms and their relation to serious games.

\subsection{Overview}
To satisfy the society's huge demand for frequent and fast game releases, modern game engines have to provide their underlying tasks in a fast and easy-to-use way.
This is why most of today's leading game engines belong to the \textit{High-Level Abstraction} category.
The most popular of them are:

\begin{itemize}
\item CryENGINE
\item Frostbite Engine
\item Source Engine
\item Unity
\item Unreal Engine (UE) / Unreal Development Kit (UDK)
\end{itemize}

\noindent
Because of the rapid evolution of game technologies, game engines often have to adapt and evolve as well.
The current version of a game engine is usually indicated by a number behind its name.
Table \ref{table:ge_overview} gives an overview of the newest versions of the aforementioned engines and shows their differences in cost and target platforms.

\noindent
\begin{minipage}{\linewidth}
	\centering
	%\bigskip
	\captionof{table}[]{ Overview of leading modern game engines}
	\label{table:ge_overview}
	\begin{tabular}{ | l | l | l | p{4cm} | p{4cm} |}
		\hline
		\multicolumn{2}{|c|}{\textbf{Engine}}&\textbf{Release}&\textbf{Prices}&\textbf{Platforms\footnotemark}\\
		\hline

	    \multirow{2}[19]{*}{\rotatebox[origin=c]{90}{CryENGINE 3}}
	    & Basic & 2011 & \parbox[t]{4cm}{free for non-commercial use} & \parbox[t]{4cm}{PS3,\\Xbox 360,\\Windows, \\Mobile in planning} \rule{0pt}{0.1ex} \\ \cline{2-5} 
	    & Pro & 2009 & Unknown & same like basic\rule[-0.4cm]{0pt}{0.0cm}\\

	    \hline
	    
	    \multirow{2}[19]{*}{\rotatebox[origin=c]{90}{Frostbite 3}}
	    & Basic & 2013 & In-house only & \parbox[t]{4cm}{PS3,\\Xbox 360,\\Windows, \\Mobile in planning} \rule[-0.10cm]{0pt}{0.0cm}\\ \cline{2-5} 
	    & Pro & - & - & - \rule[-0.4cm]{0pt}{0.0cm}\\ 
	    
	    
	    \hline
	    
	    \multirow{2}[25]{*}{\rotatebox[origin=c]{90}{Source Engine}}
 	    & Basic & 2006 & One Source based game required & Dependent on the game \rule[-0.30cm]{0pt}{0.0cm}\\ \cline{2-5} 
 	    & Pro & 2004 & Unknown & \parbox[t]{4cm}{Windows, \\Mac OS X, \\Linux, \\PS3, \\Xbox 360} \rule[-0.10cm]{0pt}{0.0cm}\\

	    \hline
	    
	    \multirow{2}[60]{*}{\rotatebox[origin=c]{90}{Unity 4}}
		& Basic & 2013 & \$0 & \parbox[t]{4cm}{Windows, \\Windows Store, \\Mac OS X, \\Linux, \\Web Player, \\Google Native Client, \\Windows Phone 8, \\iOS, \\Android, \\Blackberry, \\PS3, \\Xbox 360, \\WiiU} \rule[-0.10cm]{0pt}{0.0cm}\\ \cline{2-5} 
		& Pro & 2013 & once \$1500 or \$75 per month & \parbox[t]{4cm}{Pro features for the basic platforms} \rule[-0.10cm]{0pt}{0.0cm}\\
	    
	    \hline
	    
	    \multirow{2}[39]{*}{\rotatebox[origin=c]{90}{UDK / UE3}}
   		& UDK & 2009 & \parbox[t]{4cm}{free for non-commercial use, otherwise 25\% of revenue and \$99 for license} & \parbox[t]{4cm}{Windows, \\Mac OS X, \\iOS} \rule[-0.70cm]{0pt}{0.0cm}\\ \cline{2-5} 
   		& UE3 & 2006 & Unknown & \parbox[t]{4cm}{same like basic plus: \\Windows Store, \\Android, \\PS3, \\PSVita, \\Xbox 360, \\WiiU} \rule[-0.10cm]{0pt}{0.0cm}\\

		\hline
	\end{tabular}
	%\bigskip
\end{minipage}
\footnotetext{To deploy to consoles (PS3, XBox360, WiiU), a special license from the respective console manufacturer is required.}

\noindent
As shown in the table, most of the companies offer their game engine in two different editions. 
While the pro (or full) editions are often only accessible and affordable for big game companies, the basic versions can be used by everybody.
Here it needs to be said that the prices for most pro editions are not published. 
Game studios that are interested in a full license for e.g. the Cry Engine, first have to contact the sales service of the selling company. 
If the game studio matches the requirements, the service responds with an offer. But these offers are mostly subject to confidentiality and may not be published. 
According to rumours, the cost for such licenses amount to several hundreds of thousands or even millions of dollars\footnote{\url{http://www.gamepur.com/news/7433-cryengine-3-license-cost-12-million.html} [January 2014].}.
Besides the costs, pro and basic editions usually differ in the services (support), the number of provided functionalities and deployment platforms, but this differs from engine to engine.



\subsection{CryENGINE 3}
The CryENGINE 3 is the third iteration of Crytek's famous game engine.
The first two versions {CryENGINE 1 and 2} were used for popular titles like \textit{FarCry} (2004) or \textit{Crysis} (2007), 
which made the engine famous because of their outstanding realism and graphics quality back in that time.
Two years after the release of version 3 in 2009, Crytek published the free CryENGINE 3 Software Development Kit (SDK).
Both editions (full and SDK) have the same amount of features but the free edition can only be used for non-commercial purposes. 
Furthermore, there is no support service for customers of the SDK. 

The engine suits very well for the development of first person shooters (FPS), which is the most common genre among the games created by this engine.
Thus, in the field of serious games, the CryENGINE 3 is mostly used for police or military training purposes. 
In 2011, for example, it has been announced that the U.S. Army has licensed the CryENGINE 3 for the development of a new soldier training system\footnote{\url{http://www.rt-immersive.com/news/cryengine-licensed-to-us-army-for-dismounted-soldier-training-system} [January 2014].}.

\subsection{Frostbite 3}
Frostbite 3 is the newest version of DICE's powerful in-house game engine.
It served for the development of many games from the famous \textit{Battlefield} series (a FPS brand).
The current version is considered to be the most advanced game engine in terms of graphics and environmental realism.
Unfortunately, there is no licensing nor free edition for other companies or independent developers.

Since the game engine is only for internal use and due to the fact that DICE currently only aims on the entertainment market, there are no serious games done with this engine yet.
If DICE released a public version in the future, it would probably, similar to the CryENGINE, also be used for training purposes.

\subsection{Source Engine}
The Source engine's initial release was in 2004 and has been developed by the game company Valve.
It was used to create many famous games such as \textit{Half-Life 2}, \textit{Counter Strike: Source} and \textit{Left 4 Dead}.
To acquire a full license of the source engine, prospective customers can contact Valve in order to receive an offer which is subject to confidentiality.
On the other hand, Valve offers different SDKs and authoring tools for each Source based game, which allow the modification of the respective game.
By the modification of those games, the customer can create new games, which are then called \textit{mods} (modifications). 
To get access to those SDKs and authoring tools, one simply needs to own the Source based game that he wants to edit.

The possibility of a game modification implies that the purpose of the game can be changed as well.
This makes the Source SDKs a valuable tool for the serious games community. 
One example for a Source modification that resulted in a serious game is the mod called \textit{Serious Gordon}, which is a game that teaches its players about hygienic measurements for kitchen employees.

\subsection{Unity 4}
\label{subs:ge_unity4}
The first version of the Unity game engine was launched in 2005. At this time it was only used to develop games for the Mac operating system.
Over time, Unity grew to a huge multi-platform game engine that supports desktop, web, mobile and console platforms. 
Today it is the game engine with the most supported target platforms.
The newest version, Unity 4.3, supports visualisation and physics features for 3D and 2D games.
Unlike most other game companies, Unity Technologies publishes the prices for its pro edition and offers it to everybody.
Furthermore, its basic edition is for free and is not restricted to non-commercial use. 
This sales model allows even small companies and independent developers to develop and sell games at very low cost.
The differences between the pro and the basic edition affect only the variety of available features. The target platforms stay the same. 

In addition to the basic and pro edition, Unity Technologies offers a bundle called \textit{Unity Pro Modeling Simulation and Traning Bundle} (MS\&T Bundle).
The MS\&T Bundle is specially designed for simulation and training games. 
Unfortunately the price for this bundle is not published and must be negotiated with the Unity Technologies sales service.

There is already a variety of serious games that have been developed with Unity.
\textit{Explore Mars 3D} for the NASA, \textit{Wolfhound} for the U.S. Army and \textit{CliniSpace} for healthcare  are only a few of them.


\subsection{Unreal Engine 3 / Unreal Development Kit}
\label{subs:ge_udk}
The Unreal Engine (UE) is one of the oldest and yet one of the most successful game engines ever.
The game company Epic Games used the first version (UE1) for its games \textit{Unreal} (1998) and \textit{Unreal Tournament} (1999), which both were very successful.
Today in 2014, the newest version, UE4, is about to release, but is currently only available for selected game companies.
That is why the UE3, which was released in 2006, is subject of this investigation.
The price for a full license has to be negotiated, is subject to confidentiality and may not be published, but rumours say that it is tremendously high\footnote{\url{https://forums.uberent.com/threads/wtf-unreal-engine-cost-uber-700-000.3476/} [January 2014].}.
For small companies and indie developers, Epic Games released a free edition of the UE3, called \textit{Unreal Development Kit} (UDK), in 2009.
The UDK can be used by everyone and allows commercial use, whereby 25\% of the revenue go to Epic Games. If going commercial, one needs to purchase the \$99 UDK license.
The features of UE3 and UDK are pretty much the same. The main differences affect the number of available deployment platforms, support service and the insight of source code.

Within the serious games community, the Unreal Engine has already been around for quite some time.
\textit{America's Army}, for example, which is one of the most famous serious games, was built with UE2 and already published in 2002.
Furthermore, Epic Games started a partnership with the serious game company Virtual Heroes ``to license its Unreal Engine to government agencies from the US and its allies"\footnote{\url{http://www.theverge.com/2012/3/28/2908051/epic-unreal-engine-3-licensed-government-agencies} [January 2014].} for serious purposes.

\section{Narrowing Down the Choice}
So far, this chapter explained what a game engine is and introduced five of the leading modern game engines.
In this section, the choice of the best suitable game engine for serious games development will be narrowed down, by sorting out three of the previously introduced engines.
The two remaining ones are then examined and compared in detail in chapter \ref{c:comparison}.

\subsection{Requirements}
\label{subs:ge_narrowing_requirements}
All of the previously mentioned game engines are very popular, modern, and highly abstractive. 
Furthermore, they all provide a similar set of features and most of them are widely used.
However, to be taken into further consideration for serious games development, the following requirements have to be met by the respective game engine:

\begin{itemize}
\item Affordability
\item Support for Mobile Platforms
\end{itemize}

\subsubsection{Affordability}
When developers or companies come to the point where they need to choose their game engine, they probably already know their budget for the development.
The budget is an important factor for the choice of the game engine.
Small companies and independent developers, for example, can usually not afford to buy a full license for one of the expensive game engines.
As a result, they have to stick to open source or low-cost solutions.
For this reason, one of the requirements for the pre-selection of the engines to compare is: affordability.

\subsubsection{Support for Mobile Platforms}
Another important aspect that affects the selection of a game engine is the support for certain target platforms.
A specific type of game may require the support of specific platforms.
A modern first person shooter, for example, usually is deployed on desktop and console platforms.
The serious games field, however, comprises a lot of different game genres, what means that the target platform can vary from game to game.
According to recent researches of Ambient Insight (an e-Learning market research company) the most used platforms for educational games (edugames) are the mobile platforms (see \cite{ambient_insight_research}).
Ambient Insight found out that $\sim$62\% of the 2012 revenues of edugames were generated by the mobile market. Furthermore, they expect an increase of this share to $\sim$84\% by the year 2017.
Because of this and due to the fact that educational games are the most common type of serious games (see section \ref{subs:most_common_genre}), the pre-selected game engines should support at least the common mobile platforms.


\subsection{Conclusion}
As mentioned in section \ref{section:ge_variety}, most of the companies offer full licenses for their game engine either at very high prices or only to certain companies.
Fortunately, four of the mentioned ones provide a free or low-cost edition of their game engine.
DICE, however, uses its Frostbite engine only internally and does not publish it at all. 
For this reason, Frostbite can not be taken into further consideration.

The four remaining ones are CryENGINE 3, Source Engine, Unity 4 and UDK.
Although Crytek is planning to integrate the support of mobile platforms into its CryENGINE, there are only two engines among the remaining ones, which currently meet this requirement: Unity 4 and the UDK.

Thus it becomes obvious that Unity 4 and the UDK seem to be the best candidates.
To find out which of them is more suitable for serious games development, a detailed comparison will follow in chapter \ref{c:comparison}.

