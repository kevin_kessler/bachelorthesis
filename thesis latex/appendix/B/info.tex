\chapter{Impress - Interactive Media Presentation Software}
\label{appendix:impress}

During his stay at CERN, the author worked on different projects.
Besides the present paper, his main project was the development of a presentation software called \textit{Impress}.

\section{What is Impress}
Impress stands for \textit{\textbf{I}nteractive \textbf{M}edia \textbf{Pres}entation \textbf{S}oftware}. 
As its name implies, it is a program that allows to create and play interactive presentations.
It is developed with Unity and by the software team of CERN's Media Lab and is intended to enhance CERN's Visit Points and exhibitions by providing an immersive and impressive way of presenting information.

The software is structured in three parts:
\begin{itemize}
\item Launcher
\item Player
\item Builder
\end{itemize}

\noindent
To give an overview of Impress' functionalities, the following subsections explain and illustrate those parts.

\subsection{Launcher}
The Launcher is the starting point of the application.
It is used to manage all the Impress presentations present at the operating machine.
There are three menu tabs at the launcher.

The first one is the \textit{Presentations Tab}.
It gives an overview of the existing presentations, and allows to remove or open the selected presentation in the Player (play) or the Builder (edit). 
Furthermore, a selected presentation can be set as default, which means that it automatically starts playing when opening Impress (useful for exhibitions). 
In addition there is the possibility of opening a video streaming presentation, which allows to stream and display the input of external video devices like web cams or screen capturers of other machines.
Figure \ref{graphic:appendix_b_launcher_presentations} shows a screenshot of the Launcher and this tab. 

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/launcher}
\caption[]{Impress Launcher - Presentations Tab}
\label{graphic:appendix_b_launcher_presentations}
\end{figure}


The second tab is the \textit{Open Presentation Tab}, which is used to add presentations to the launcher by browsing through the system's files and directories.
The supported formats for opening an Impress presentation are \textit{.pdf}, \textit{.imp} (Impress package), and presentation folders (similar to project folders of other applications).

The third and last tab of the Launcher is the \textit{Settings Tab}.
It comprises several controls to change player specific preferences, such as volume, resolution, quality, and so on.

Both tabs are illustrated by screenshots in figure \ref{graphic:appendix_b_launcher_open_and_settings}.

To keep visitors of exhibitions from making any changes to the presentations or the system, the launcher can be switched into \textit{Exhibition Mode} by pressing CTRL+E.
Thus, only the \textit{Presentations Tab} is accessible and only the play button is shown in order to allow switching between presentations.


\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/launcher_open_and_settings}
\caption[]{Impress Launcher - Open Presentation Tab (left) and Settings Tab (right)}
\label{graphic:appendix_b_launcher_open_and_settings}
\end{figure}


\subsection{Player}
The Player is the part where the actual presentation plays (runtime of the presentation).

A common Impress presentation is made of slides, whereby each slide has a number of containers.
These containers are used to define the layout and position of the displayed information.
Thus, each container can contain several media elements and, depending on its type, arrange them in different ways.
Currently, four types of containers, which can be seen in figure \ref{graphic:appendix_b_layouts}, are supported.
While the \textit{Rectangle} container can only show one media element at once, all the others can arrange their media elements specifically.

Any container can contain an arbitrary amount of media elements, whereby the media types do not matter. This means a container can have any combination of different media.
Besides standard media elements like images, videos and texts, Impress supports websites, 3D scenes and audio files (see also figure \ref{graphic:appendix_b_media}).
\clearpage

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/layouts}
\caption[]{Impress Container Types}
\label{graphic:appendix_b_layouts}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/media}
\caption[]{Impress Media Types}
\label{graphic:appendix_b_media}
\end{figure}

\clearpage

Furthermore, all elements of a slide can be interactively controllable via mouse and/or touch during presentation runtime.
The controllability of an element depends on its type. 
Each individual container can be enabled to be moveable, scalable, rotatable, maximizable, and closable, whereas specific container types come with additional controls.
A gallery, for example, provides navigation arrows which allow to browse through the containing media elements.
But not only containers come with specific controls, media element controls also differ depending on their type.
While a video, for instance, provides controls to play, pause or stop it, a 3D scene offers the possibility to zoom in/out and/or turn its objects around.

Another feature that supports the interactivity of a presentation at runtime is the event mechanism of Impress.
An event represents a certain action like play video, change slide, move container, change language, and so on. 
Depending on their type, slides and media elements have specific triggers to which events can be attached.
An image, for example, may have an event ``Go To Slide 1" attached to its \textit{OnClick} trigger, which causes the presentation's current slide to change when clicking on that image.

Together, the combination of controllable elements and independent events allow to create an absolutely interactive presentation.


\subsection{Builder}
The Builder is the WYSIWYG editor of Impress. 
It is the part where presentations are created and/or edited, which makes it to the most complex part of Impress.
It consists of several panel's which provide specific controls to manipulate the elements of a presentation or to open certain dialog boxes.
The following list comprises the important parts of the Builder, including panels, dialog boxes and more:

\begin{itemize}
\item A. File
\item B. Slide View
\item C. Slides 
\item D. Slide Preferences
\item E. Container Preferences
\item F. Media Preferences
\item G. Basic Media
\item H. Imported Media
\item I. Container List
\item J. Media List
\item K. Add Media
\item L. Events
\item M. Slide Events
\item N. Media Events
\item O. Create/Edit Event
\item P. Add Events
\item Q. Layout Settings
\item R. Presentation Preferences
\end{itemize}

Below there are short descriptions of the aforementioned parts and several screenshots that illustrate them.

\subsubsection{Parts A. to G.}
The parts A. to G. are illustrated in figure \ref{graphic:appendix_b_builder_elementprefs}.
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/builder01}
\caption[]{Impress Builder - File, Slide View, Slides, Content Preferences, and Basic Media}
\label{graphic:appendix_b_builder_elementprefs}
\end{figure}

\noindent
At the top of the screenshot there is the \textit{File Panel}. 
It contains several buttons that are used to trigger the following actions (from left to right): New, Open, Save, Save As, Preferences, Preview, About, and Back To Launcher.

The \textit{Slide View} (arranged in the centre of the Builder) presents the content of the current selected slide and allows to select, arrange, scale, and rotate its containers.

On the left of the Builder, one can see the \textit{Slides Panel}.
It gives an overview of the existing slides of the current presentation and provides buttons to create, copy, move, and delete them.
Furthermore, it can be used to select the current slide to work on.

The \textit{Slide Preferences Panel} in the lower left corner allows to change title, description, and background of the current slide.

In the lower centre there is the \textit{Container Preferences Panel}, which can be used to adjust the position, dimensions and rotation of the selected container.
Furthermore, it contains the button that opens the \textit{Layout Settings Dialog Box}, which will be introduced later in this chapter.

The \textit{Media Preferences Panel} in the lower right corner allows among others to change the title and description of a media element.
Furthermore, depending on the type of the selected media element, there are specific settings such as the \textit{Sourcefile} one, which allows to change the source file of an image or video.

To add basic media elements (media that does not require any source file), the user can simply drag and drop an entry from the \textit{Basic Media Panel} (on the right of the Builder) to the \textit{Slide View}.



\subsubsection{Parts H. to K.}
The parts H. to J. are illustrated (among others) in figure \ref{graphic:appendix_b_builder_contentlists}.
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/builder02}
\caption[]{Impress Builder - Content Lists and Imported Media}
\label{graphic:appendix_b_builder_contentlists}
\end{figure}

\noindent
Similar to the \textit{Slides Panel}, the \textit{Container List Panel} (placed in the lower left) gives an overview of all containers of the current slide.
It can be used to select, copy and delete containers. The order indication tells the user on which layer the respective container lies.
This means that a container with a lower order will appear behind containers with higher orders, if they are positioned at the same area.
The order can be changed by using the arrow buttons of the panel.

The \textit{Media List Panel} displays all media elements of the currently selected container.
Moreover, it allows to select, add and remove media elements from the container. The arrow buttons can be used to change the order of appearance of the media elements in the container.

To add media that rely on source files, such as images, videos, audio, and 3D, to the presentation, the \textit{Imported Media Panel} on the right of the Builder must be used.
It provides a button that allows to chose and import source files from the user's hard disk and displays all of the already imported ones.
Similar to the \textit{Basic Media Panel}, elements can simply be dragged and dropped to the \textit{Slide View} in order to place them at a slide.
Furthermore, multiple media elements can be added at once by using the \textit{Add Media Dialog Box} (Part K.), illustrated in figure \ref{graphic:appendix_b_add_media}.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/add_media}
\caption[]{Impress Builder - Add Media Dialog Box}
\label{graphic:appendix_b_add_media}
\end{figure}

\subsubsection{Parts L. to N.}
The parts L. to N. are illustrated (among others) in figure \ref{graphic:appendix_b_builder_events}.
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/builder03}
\caption[]{Impress Builder - Events}
\label{graphic:appendix_b_builder_events}
\end{figure}

The \textit{Events Panel} on the right of the builder gives an overview of all the created events of the current presentation.
The drop-down boxes at the top of the panel allow to filter the displayed events by type or action.
Furthermore, there are buttons that allow to create, edit, copy, and delete a selected event.

To attach an event to a slide or a media element, it can simply be dragged and dropped in one of the respective event list panels (\textit{Slide Events Panel} on the lower left, \textit{Media Events Panel} on the lower right, and \textit{3D Events Panel} next to it).
The event list panels are all working in the same way. On the left of the panel, there is always the list of available triggers. One of them must be selected in order to be able to attach events to it.
Besides the drag and drop, there is also the alternative to attach events by using the panel buttons.

\subsubsection{Parts O. and P.}
The dialog box illustrated in figure \ref{graphic:appendix_b_create_event} is used to create and/or edit events.
As shown in the figure, the users need to choose the desired event type and action, before they can set event specific properties.
The case illustrated in the figure, shows the creation of a \textit{PlayPauseStop Event}. 
In the properties part of this event, the users need to select the video or audio element of a slide's container, which is then affected when the event gets triggered.

\clearpage

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/create_event}
\caption[]{Impress Builder - Create Event Dialog Box}
\label{graphic:appendix_b_create_event}
\end{figure}

\noindent
The \textit{Add Events Dialog Box}, which is shown in figure \ref{graphic:appendix_b_add_events}, provides yet another possibility to attach events to a selected trigger.
The advantage of this method is that multiple events can be attached at once.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/add_events}
\caption[]{Impress Builder - Add Events Dialog Box}
\label{graphic:appendix_b_add_events}
\end{figure}

\subsubsection{Part Q}
The \textit{Layout Settings Dialog}, shown in figure \ref{graphic:appendix_b_layout_settings}, is used to set layout specific preferences of the selected container.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/layout_settings}
\caption[]{Impress Builder - Layout Settings Dialog Box}
\label{graphic:appendix_b_layout_settings}
\end{figure}

\noindent
As illustrated, it provides the possibility to change a container's type, interactivity behaviour and look.
The specific layout settings on the right of the dialog box, depend on the selected layout type (Rectangle, Gallery, Ring, Thumbnails).

\subsubsection{Part R}
The last important part to mention is the \textit{Presentation Preferences Dialog Box}, shown in figure \ref{graphic:appendix_b_pres_prefs}.
It is used to set the global settings of the current presentation and is divided into four parts.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{appendix_b/presentation_prefs}
\caption[]{Impress Builder - Presentation Preferences Dialog Box}
\label{graphic:appendix_b_pres_prefs}
\end{figure}



The general part comprises the values of name, author, and thumbnail, which are used to identify the presentation among others in the Launcher of Impress.
The aspect ratio attribute defines the ratio of the \textit{Slide View}, which allows to tailor presentations for any target display.
At the bottom of the general preferences, one can see the control slide feature. This feature allows to define elements on a slide, which stay always on top of each other slide.
Thus, controls that are used during the whole presentation only need to be defined once rather than for every slide.

The languages part of the dialog box allows to manage the different languages of the current presentation.
This means that a presentation may have multiple languages, defined by the user. 
Thereby it is possible to change the presentation's language via event on runtime, which is extremely useful for presentations running in an exhibition.

The OSC parts of the presentation preferences, are used to set up OSC specific settings.
OSC stands for \textit{Open Sound Control} and is the name of a protocol used for messaging among computers and other multimedia devices.
The support of this protocol, allows Impress to communicate to other machines running Impress.
Thus, a presentation on one machine can control the presentation of another machine by using OSC events.
Furthermore, in the case of CERN, it allows to send and receive commands to/from control system devices, in order to control hardware elements, such as lights in the room of the exhibition, from within the presentation. 

\section{Personal Contribution}
During the author's stay at CERN, the software team of the Media Lab consisted of three developers, including the author.
Accordingly, during that period, Impress was developed by a group of three persons, whereby each of them was responsible for different parts of the development.

The parts for which the author had been responsible, are listed and briefly explained below:
\begin{itemize}
\item \textbf{Planning} - Concept and Design of Impress (shared task).
\item \textbf{Modelling} - Design and implementation of the presentation data structure.
\item \textbf{IO} - Saving and loading of presentation files. Includes design and implementation of the presentation folder and file structure. This is mostly based on XML files and was realised by using LINQ which is one of the .NET Framework's components. 
\item \textbf{File Import} - Implementation of the import of source files for media elements (images, videos, etc.). Realised by using the IO functions of the .NET Framework.
\item \textbf{Event Mechanism} - Design and implementation of Impress' event logic.
\item \textbf{Language Mechanism} - Design and implementation of the presentation language feature, which allows to have multiple languages per presentation and change them on runtime.
\item \textbf{Launcher} - Design and Development of the Impress Launcher (shared task). Specific parts of the author:
	\begin{itemize}
	\item Logic of the presentation thumbnails (remembering the added ones and accessing the information of the presentations)
	\item Implementation of the exhibition mode and the switch mechanism between both modes (normal and exhibition)
	\item File Browser (shared task)
	\end{itemize}

\item \textbf{Builder} - Design and development of the Impress Builder, including logic and GUI (shared task). Specific parts of the author:
	\begin{itemize}
	\item Presentation Preferences Dialog Box
	\item File Browser Dialog Box (shared task)
	\item Save As Dialog Box
	\item Warning Dialog Boxes
	\item Slide Preferences Panel
	\item Create / Edit Event Dialog Box
	\item Add Events Dialog Box
	\item Add Media Dialog Box
	\item Content List Panels (Container List, Media List)
	\item Event List Panels (Events, Slide Events, Media Events, 3D Events)
	\item Basic Media Panel and Imported Media Panel
	\item File Panel
	\item Drag and Drop of media elements and events (shared task)
	\item Scalability and remembering of panel sizes
	\end{itemize}

\item \textbf{OSC} - Implementation of the interplay between the OSC feature and the event mechanism
\item \textbf{Quality} - Introducing and setting up a revision control and source code management system (Git), an issue tracking system (Bugzilla, later JIRA) and a documentation generation tool (Doxygen) for the development team.
\end{itemize}

\section{Current State and Future Work}
At the time of writing these lines, the Launcher and the Player of Impress are already in use at CERN's exhibitions and Visit Points.
They are currently mainly used by guides in order to support their talks while on a tour with a group of visitors.
In addition, individual persons also make use of Impress presentations. 
In January 2014, for example, CERN's Director General Rolf Heuer, used it to welcome and inform the Dutch State Secretary of the Ministry of Education, Culture and Science.

The Builder of Impress is currently in the closed beta phase.
This means it is tested and adjusted internally by the members of the Media Lab and is not available for the public yet.
Thus, Impress presentations are currently only created by the Media Lab.
In future, Impress, including Launcher, Player and Builder, is foreseen to be used CERN-wide.
Thus, the members of CERN's visitor centres as well as individual persons will be able to create and use their own presentations with Impress.

During the internal tests, several issues and feature requests have already been reported to the issue tracking system.
Those will be fixed / implemented before Impress becomes available for the public.

Platform wise, Impress is currently only supported by windows, because platform specific plugins were used for the development.
One goal of future development is to replace those plugins and thereby enable it for usage on Mac OS and Linux.













