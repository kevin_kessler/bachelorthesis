\select@language {english}
\select@language {ngerman}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Goals}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Structure of the Paper}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Allocation of Project Report and Bachelor's Thesis}{4}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Project Report}{4}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Bachelor's Thesis}{4}{subsection.1.4.2}
\contentsline {chapter}{\numberline {2}CERN and Education}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}About CERN}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Education at CERN}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Teacher Programme}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Guided Tours}{7}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Visit Points}{7}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Exhibitions}{7}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Combining Education with Interactive Applications}{7}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Proton Football}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Higgnite}{8}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Serious Games}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}What is a Serious Game}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Definition}{9}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Purposes and Classification}{10}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}The most Common Type}{11}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Benefits of Usage}{14}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Serious Games as Educational Concept}{14}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Games as Part of our Society}{16}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Positive Impact}{16}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Safety and Cost}{17}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Privacy and Feedback}{17}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Didactic}{17}{subsection.3.2.6}
\contentsline {subsubsection}{Support of other learning concepts and new qualities of learning}{18}{section*.10}
\contentsline {subsubsection}{A different organisation of learning}{18}{section*.11}
\contentsline {subsubsection}{Shortened learning periods}{18}{section*.12}
\contentsline {section}{\numberline {3.3}History}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Pre-Electronic Era}{18}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Computer Era}{19}{subsection.3.3.2}
\contentsline {chapter}{\numberline {4}Game Engines}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}What is a Game Engine}{21}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Components}{22}{subsection.4.1.1}
\contentsline {subsubsection}{Graphics Engine}{22}{section*.15}
\contentsline {subsubsection}{AI Engine}{23}{section*.16}
\contentsline {subsubsection}{Physics Engine}{23}{section*.17}
\contentsline {subsubsection}{Sound Engine}{24}{section*.18}
\contentsline {subsubsection}{User Control System}{24}{section*.19}
\contentsline {subsection}{\numberline {4.1.2}Types}{24}{subsection.4.1.2}
\contentsline {subsubsection}{Low-Level Abstraction}{25}{section*.20}
\contentsline {subsubsection}{Mid-Level Abstraction}{25}{section*.21}
\contentsline {subsubsection}{High-Level Abstraction}{26}{section*.22}
\contentsline {section}{\numberline {4.2}Variety}{26}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Overview}{26}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}CryENGINE 3}{28}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Frostbite 3}{28}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Source Engine}{29}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Unity 4}{29}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Unreal Engine 3 / Unreal Development Kit}{30}{subsection.4.2.6}
\contentsline {section}{\numberline {4.3}Narrowing Down the Choice}{30}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Requirements}{30}{subsection.4.3.1}
\contentsline {subsubsection}{Affordability}{31}{section*.23}
\contentsline {subsubsection}{Support for Mobile Platforms}{31}{section*.24}
\contentsline {subsection}{\numberline {4.3.2}Conclusion}{31}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Comparison between Unity and UDK}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}Method of Comparison}{33}{section.5.1}
\contentsline {section}{\numberline {5.2}Comparison Aspects}{34}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Development}{34}{subsection.5.2.1}
\contentsline {subsubsection}{Weighting of Environment}{35}{section*.26}
\contentsline {subsubsection}{Weighting of Scripting}{35}{section*.27}
\contentsline {subsubsection}{Weighting of Debugging}{36}{section*.28}
\contentsline {subsection}{\numberline {5.2.2}Functionalities}{36}{subsection.5.2.2}
\contentsline {subsubsection}{Weighting of Graphics}{36}{section*.29}
\contentsline {subsubsection}{Weighting of Physics}{36}{section*.30}
\contentsline {subsubsection}{Weighting of Audio}{37}{section*.31}
\contentsline {subsubsection}{Weighting of Artificial Intelligence}{37}{section*.32}
\contentsline {subsection}{\numberline {5.2.3}Compatibility}{37}{subsection.5.2.3}
\contentsline {subsubsection}{Weighting of Assets}{37}{section*.33}
\contentsline {subsubsection}{Weighting of Platforms}{38}{section*.34}
\contentsline {subsubsection}{Weighting of Input}{38}{section*.35}
\contentsline {subsection}{\numberline {5.2.4}GUI Creation}{38}{subsection.5.2.4}
\contentsline {subsubsection}{Weighting of Control Elements}{38}{section*.36}
\contentsline {subsubsection}{Weighting of Information Display}{39}{section*.37}
\contentsline {subsubsection}{Weighting of Tools}{39}{section*.38}
\contentsline {subsection}{\numberline {5.2.5}Support}{39}{subsection.5.2.5}
\contentsline {subsubsection}{Weighting of Documentation}{40}{section*.39}
\contentsline {subsubsection}{Weighting of Community}{40}{section*.40}
\contentsline {section}{\numberline {5.3}Development}{40}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Environment}{40}{subsection.5.3.1}
\contentsline {subsubsection}{Unity}{40}{section*.41}
\contentsline {subsubsection}{UDK}{42}{section*.43}
\contentsline {subsection}{\numberline {5.3.2}Scripting}{42}{subsection.5.3.2}
\contentsline {subsubsection}{Unity}{42}{section*.45}
\contentsline {subsubsection}{UDK}{43}{section*.46}
\contentsline {subsection}{\numberline {5.3.3}Debugging}{44}{subsection.5.3.3}
\contentsline {subsubsection}{Unity}{44}{section*.47}
\contentsline {subsubsection}{UDK}{44}{section*.48}
\contentsline {section}{\numberline {5.4}Functionalities}{44}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Graphics}{45}{subsection.5.4.1}
\contentsline {subsubsection}{Unity}{45}{section*.49}
\contentsline {subsubsection}{UDK}{45}{section*.50}
\contentsline {subsection}{\numberline {5.4.2}Physics}{45}{subsection.5.4.2}
\contentsline {subsubsection}{Unity}{45}{section*.51}
\contentsline {subsubsection}{UDK}{46}{section*.52}
\contentsline {subsection}{\numberline {5.4.3}Audio}{46}{subsection.5.4.3}
\contentsline {subsubsection}{Unity}{46}{section*.53}
\contentsline {subsubsection}{UDK}{46}{section*.54}
\contentsline {subsection}{\numberline {5.4.4}Artificial Intelligence}{47}{subsection.5.4.4}
\contentsline {subsubsection}{Unity}{47}{section*.55}
\contentsline {subsubsection}{UDK}{47}{section*.56}
\contentsline {section}{\numberline {5.5}Compatibility}{47}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Assets}{47}{subsection.5.5.1}
\contentsline {subsubsection}{Unity}{47}{section*.57}
\contentsline {subsubsection}{UDK}{48}{section*.58}
\contentsline {subsection}{\numberline {5.5.2}Platforms}{48}{subsection.5.5.2}
\contentsline {subsubsection}{Unity}{48}{section*.59}
\contentsline {subsubsection}{UDK}{49}{section*.60}
\contentsline {subsection}{\numberline {5.5.3}Input}{49}{subsection.5.5.3}
\contentsline {subsubsection}{Unity}{49}{section*.61}
\contentsline {subsubsection}{UDK}{50}{section*.62}
\contentsline {section}{\numberline {5.6}GUI Creation}{50}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Control Elements}{51}{subsection.5.6.1}
\contentsline {subsubsection}{Unity}{51}{section*.63}
\contentsline {subsubsection}{UDK}{51}{section*.64}
\contentsline {subsection}{\numberline {5.6.2}Information Display}{52}{subsection.5.6.2}
\contentsline {subsubsection}{Unity}{52}{section*.65}
\contentsline {subsubsection}{UDK}{52}{section*.66}
\contentsline {subsection}{\numberline {5.6.3}Tools}{52}{subsection.5.6.3}
\contentsline {subsubsection}{Unity}{53}{section*.67}
\contentsline {subsubsection}{UDK}{53}{section*.68}
\contentsline {section}{\numberline {5.7}Support}{53}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Documentation}{53}{subsection.5.7.1}
\contentsline {subsubsection}{Unity}{53}{section*.69}
\contentsline {subsubsection}{UDK}{54}{section*.70}
\contentsline {subsection}{\numberline {5.7.2}Community}{54}{subsection.5.7.2}
\contentsline {subsubsection}{Unity}{54}{section*.71}
\contentsline {subsubsection}{UDK}{55}{section*.72}
\contentsline {section}{\numberline {5.8}Comparison Results}{55}{section.5.8}
\contentsline {chapter}{\numberline {6}Development of a Serious Game with Unity}{57}{chapter.6}
\contentsline {section}{\numberline {6.1}Development Process}{57}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Roles}{57}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Process Model}{59}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Concept Development of Freeq}{60}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Description of the Idea}{60}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Goals}{61}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Genre, Audience and Platforms}{61}{subsection.6.2.3}
\contentsline {section}{\numberline {6.3}Design of Freeq}{61}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Requirements}{61}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Gameplay and Game Flow}{62}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Implementation of Freeq}{63}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Modelling}{63}{subsection.6.4.1}
\contentsline {subsubsection}{Model}{64}{section*.76}
\contentsline {subsubsection}{View}{65}{section*.78}
\contentsline {subsubsection}{Controller}{66}{section*.80}
\contentsline {subsection}{\numberline {6.4.2}Realisation}{67}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Prototype}{71}{subsection.6.4.3}
\contentsline {section}{\numberline {6.5}Testing of Freeq}{75}{section.6.5}
\contentsline {section}{\numberline {6.6}Deployment of Freeq}{75}{section.6.6}
\contentsline {section}{\numberline {6.7}Evaluation and Conclusion}{76}{section.6.7}
\contentsline {chapter}{\numberline {7}Conclusion}{81}{chapter.7}
\contentsline {section}{\numberline {7.1}Results}{81}{section.7.1}
\contentsline {section}{\numberline {7.2}Prospect}{82}{section.7.2}
\contentsline {chapter}{\numberline {A}Scribbles of Freeq's Design Process}{85}{appendix.A}
\contentsline {chapter}{\numberline {B}Impress - Interactive Media Presentation Software}{89}{appendix.B}
\contentsline {section}{\numberline {B.1}What is Impress}{89}{section.B.1}
\contentsline {subsection}{\numberline {B.1.1}Launcher}{89}{subsection.B.1.1}
\contentsline {subsection}{\numberline {B.1.2}Player}{90}{subsection.B.1.2}
\contentsline {subsection}{\numberline {B.1.3}Builder}{93}{subsection.B.1.3}
\contentsline {subsubsection}{Parts A. to G.}{94}{section*.96}
\contentsline {subsubsection}{Parts H. to K.}{95}{section*.98}
\contentsline {subsubsection}{Parts L. to N.}{96}{section*.101}
\contentsline {subsubsection}{Parts O. and P.}{97}{section*.103}
\contentsline {subsubsection}{Part Q}{99}{section*.106}
\contentsline {subsubsection}{Part R}{99}{section*.108}
\contentsline {section}{\numberline {B.2}Personal Contribution}{100}{section.B.2}
\contentsline {section}{\numberline {B.3}Current State and Future Work}{102}{section.B.3}
\contentsline {chapter}{\numberline {C}Overview CD-ROM}{103}{appendix.C}
\contentsline {chapter}{Bibliography}{104}{appendix.C}
\select@language {english}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {german}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {ngerman}
\select@language {english}
