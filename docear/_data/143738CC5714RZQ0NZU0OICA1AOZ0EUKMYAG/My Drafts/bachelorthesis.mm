<map version="docear 1.1" dcr_id="1389213143370_1ju72ftxl3j4lwxhw243ks8h6" project="143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG" project_last_home="file:/E:/Studium/6.%20Semester/BA/Thesis/docear/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<attribute_registry SHOW_ATTRIBUTES="hide"/>
<node TEXT="bachelorthesis" FOLDED="false" ID="ID_310142428" CREATED="1389213080369" MODIFIED="1389213391814">
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<hook NAME="MapStyle" zoom="0.829">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Serious Games" POSITION="right" ID="ID_649148863" CREATED="1389213401584" MODIFIED="1389284704335">
<edge COLOR="#ff0000"/>
<attribute NAME="mon_incoming_folder" VALUE="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/" OBJECT="java.net.URI|project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/"/>
<attribute NAME="mon_mindmap_folder" VALUE="@@library_mindmaps@@"/>
<attribute NAME="mon_auto" VALUE="2" OBJECT="java.lang.Integer|2"/>
<attribute NAME="mon_subdirs" VALUE="2" OBJECT="java.lang.Integer|2"/>
<attribute NAME="mon_add_extra_incoming_node" VALUE="2" OBJECT="java.lang.Integer|2"/>
<attribute NAME="mon_flatten_dirs" VALUE="2" OBJECT="java.lang.Integer|2"/>
<node TEXT="Incoming" ID="ID_620189859" CREATED="1389213419143" MODIFIED="1389213419227" INCOMING="true" COLOR="#ffffff" BACKGROUND_COLOR="#006699">
<font NAME="Courier New" BOLD="true"/>
<node TEXT="Digitalarti_Mag_No_0_high_res.pdf" ID="ID_1181163762" CREATED="1389213419293" MODIFIED="1389213419293" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Digitalarti_Mag_No_0_high_res.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="15EAAB5AF3EAB1F682AC71576D07F38CB6D9C85FB6E29F965F131253C8ED46">
    <pdf_title>RYOICHI KUROKAWA</pdf_title>
</pdf_annotation>
<node TEXT="games develop children&#xd;&#xa;various cognitive faculties: reading, being&#xd;&#xa;logical, observation capacities, maps &#xd;&#xa;reading, orientation, taking notes, problems&#xd;&#xa;resolution and last but not least working&#xd;&#xa;in a team" ID="ID_1822527036" CREATED="1389213419314" MODIFIED="1389213419314" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Digitalarti_Mag_No_0_high_res.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="4802719356264744914" object_number="119" document_hash="15EAAB5AF3EAB1F682AC71576D07F38CB6D9C85FB6E29F965F131253C8ED46">
    <pdf_title>RYOICHI KUROKAWA</pdf_title>
</pdf_annotation>
</node>
<node TEXT="being part of a team, respect some rules,&#xd;&#xa;listen to the others to work together, to&#xd;&#xa;develop strategies and finally progress in&#xd;&#xa;the game and win" ID="ID_1484922708" CREATED="1389213419234" MODIFIED="1389213419235" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Digitalarti_Mag_No_0_high_res.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="735040236004397434" object_number="120" document_hash="15EAAB5AF3EAB1F682AC71576D07F38CB6D9C85FB6E29F965F131253C8ED46">
    <pdf_title>RYOICHI KUROKAWA</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="Serious Games Development (sg notes).pdf" ID="ID_338001913" CREATED="1389284641416" MODIFIED="1389284641416" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
<node TEXT="explaining games, going from games to serious games." ID="ID_129021758" CREATED="1389284647909" MODIFIED="1389284647909" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="25" object_id="610614268468963813" object_number="393" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="In  this  work  I  define  games  as  digital  programs,  running  on  personal  computers &#xd;&#xa;and/or consoles (with or without specialized peripheral devices)" ID="ID_1065707911" CREATED="1389284647941" MODIFIED="1389284647941" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="7898179673618023877" object_number="395" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" In  this  context,  health  games  are &#xd;&#xa;games used for improving the health and wellbeing of individuals." ID="ID_576127052" CREATED="1389284647973" MODIFIED="1389284647973" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="8994975258845488608" object_number="396" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" This positions health games within the serious game paradigm, which emerged &#xd;&#xa;in  the  recent  decade  and  calls  for  using  games  and  game-based  techniques  for  the &#xd;&#xa;purposes outside entertainment. " ID="ID_654950618" CREATED="1389284648004" MODIFIED="1389284648004" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="7075906975196315205" object_number="397" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="ben sawyer: &quot;games that are for &apos;serious&apos; (non-entertainment) purpose." ID="ID_1055361786" CREATED="1389284648037" MODIFIED="1389284648037" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="25" object_id="1442066559664759321" object_number="398" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="games classification: use of sawyer and smith&apos;s taxonomy" ID="ID_1380060697" CREATED="1389284648067" MODIFIED="1389284648067" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="28" object_id="79647610698067637" object_number="407" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Sawyer and Smith&#x2019;s taxonomy, is not the only attempt to create a clear definition &#xd;&#xa;of the field. In their report on serious gaming industry, Michaud and his associates &#xd;&#xa;[19] separate the serious game industry into 4 distinct segments, defined by end cus-&#xd;&#xa;tomers,  being  Healthcare,  Teaching  and  Training,  Public  Information  and  Business &#xd;&#xa;and Defense and Public Safety." ID="ID_639719066" CREATED="1389284648098" MODIFIED="1389284648098" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="29" object_id="575018430334996805" object_number="411" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="nice idea for INTRODUCTION" ID="ID_1025301622" CREATED="1389284647842" MODIFIED="1389284647842" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="53" object_id="5376351718837700595" object_number="929" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Given the high level of &#xd;&#xa;engagement that people exhibit whilst playing games it seems logical that combining &#xd;&#xa;the  unique  characteristics  that  make  these  games  so  engaging  with  conventional &#xd;&#xa;methodologies in education could provide a powerful means of encouraging students &#xd;&#xa;more effectively in learning activities. This concept forms the underlying principle &#xd;&#xa;of  the  idea  of  serious  games  coined  by  Sawyer  and  Rejeski  [25]." ID="ID_1624761998" CREATED="1389284647810" MODIFIED="1389284647810" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="53" object_id="4860198948025671295" object_number="931" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="goal: offer as a case study" ID="ID_655337667" CREATED="1389284647777" MODIFIED="1389284647777" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="54" object_id="567198389766550927" object_number="948" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" The  goal  is  to  offer  this  as  a  case &#xd;&#xa;study for those wishing to create 3D educational content within an academic context" ID="ID_1287215566" CREATED="1389284647748" MODIFIED="1389284647748" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="54" object_id="2796701228486446284" object_number="950" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="There is a large amount of evidence available to suggest that the use of serious games &#xd;&#xa;for learning can make positive impacts on learning outcomes at all levels of educa-&#xd;&#xa;tion,  ranging  from  primary  [6],  secondary  [19],  higher  education  [7],  [14],  and  for &#xd;&#xa;children with special educational needs [35]." ID="ID_1369233763" CREATED="1389284647717" MODIFIED="1389284647717" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="56" object_id="3601264766204391782" object_number="959" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Serious Games are games that &#xd;&#xa;educate, train and inform using entertainment principles, creativity,  and tech-&#xd;&#xa;nology." ID="ID_1552575179" CREATED="1389284647683" MODIFIED="1389284647684" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="84" object_id="573358386031117641" object_number="1096" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" A serious  game is a &#xd;&#xa;game designed for a primary purpose other than pure entertainment. Serious games &#xd;&#xa;can  be  applied  to  a  broad  spectrum  of  application  areas,  e.g.  military,  government, &#xd;&#xa;educational, corporate, and healthcare." ID="ID_838613123" CREATED="1389284647652" MODIFIED="1389284647652" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="84" object_id="7794920846928164575" object_number="1097" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" It is the addition of pedagogy (activities that educate &#xd;&#xa;or  instruct,  thereby  imparting  knowledge  or  skill)  that  makes  games  serious." ID="ID_743364466" CREATED="1389284647620" MODIFIED="1389284647620" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="84" object_id="5802563777661345267" object_number="1098" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="4.1  Serious Games Awareness &#xd;&#xa;According  to  our  survey  almost  29%  of  companies  responded  that  they  have   &#xd;&#xa;heard about the use of Serious Games for corporate training. However, 15 out of 21 " ID="ID_1534130078" CREATED="1389284647587" MODIFIED="1389284647587" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="88" object_id="1532720562655866818" object_number="1127" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="serious game vs teacher" ID="ID_992646090" CREATED="1389284647555" MODIFIED="1389284647555" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="124" object_id="1615764751321596691" object_number="4866" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Opinions on Bringing Serious Games into the Classroom. " ID="ID_337285674" CREATED="1389284647524" MODIFIED="1389284647524" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="127" object_id="5924916469437879472" object_number="4876" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="explaining games, going from games to serious games." ID="ID_1701327226" CREATED="1389284911350" MODIFIED="1389284911351" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="25" object_id="610614268468963813" object_number="393" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="In  this  work  I  define  games  as  digital  programs,  running  on  personal  computers &#xd;&#xa;and/or consoles (with or without specialized peripheral devices)" ID="ID_398053791" CREATED="1389284911370" MODIFIED="1389284911370" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="7898179673618023877" object_number="395" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" In  this  context,  health  games  are &#xd;&#xa;games used for improving the health and wellbeing of individuals." ID="ID_533601917" CREATED="1389284911393" MODIFIED="1389284911393" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="8994975258845488608" object_number="396" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" This positions health games within the serious game paradigm, which emerged &#xd;&#xa;in  the  recent  decade  and  calls  for  using  games  and  game-based  techniques  for  the &#xd;&#xa;purposes outside entertainment. " ID="ID_338950098" CREATED="1389284911415" MODIFIED="1389284911415" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="25" object_id="7075906975196315205" object_number="397" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="ben sawyer: &quot;games that are for &apos;serious&apos; (non-entertainment) purpose." ID="ID_568978929" CREATED="1389284911440" MODIFIED="1389284911440" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="25" object_id="1442066559664759321" object_number="398" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="games classification: use of sawyer and smith&apos;s taxonomy" ID="ID_653950640" CREATED="1389284911459" MODIFIED="1389284911459" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="28" object_id="79647610698067637" object_number="407" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Sawyer and Smith&#x2019;s taxonomy, is not the only attempt to create a clear definition &#xd;&#xa;of the field. In their report on serious gaming industry, Michaud and his associates &#xd;&#xa;[19] separate the serious game industry into 4 distinct segments, defined by end cus-&#xd;&#xa;tomers,  being  Healthcare,  Teaching  and  Training,  Public  Information  and  Business &#xd;&#xa;and Defense and Public Safety." ID="ID_1808074959" CREATED="1389284911478" MODIFIED="1389284911478" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="29" object_id="575018430334996805" object_number="411" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="nice idea for INTRODUCTION" ID="ID_1586169243" CREATED="1389284911501" MODIFIED="1389284911501" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="53" object_id="5376351718837700595" object_number="929" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Given the high level of &#xd;&#xa;engagement that people exhibit whilst playing games it seems logical that combining &#xd;&#xa;the  unique  characteristics  that  make  these  games  so  engaging  with  conventional &#xd;&#xa;methodologies in education could provide a powerful means of encouraging students &#xd;&#xa;more effectively in learning activities. This concept forms the underlying principle &#xd;&#xa;of  the  idea  of  serious  games  coined  by  Sawyer  and  Rejeski  [25]." ID="ID_1494204381" CREATED="1389284911521" MODIFIED="1389284911521" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="53" object_id="4860198948025671295" object_number="931" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="goal: offer as a case study" ID="ID_282779543" CREATED="1389284911543" MODIFIED="1389284911543" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="54" object_id="567198389766550927" object_number="948" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" The  goal  is  to  offer  this  as  a  case &#xd;&#xa;study for those wishing to create 3D educational content within an academic context" ID="ID_510534046" CREATED="1389284911563" MODIFIED="1389284911563" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="54" object_id="2796701228486446284" object_number="950" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="There is a large amount of evidence available to suggest that the use of serious games &#xd;&#xa;for learning can make positive impacts on learning outcomes at all levels of educa-&#xd;&#xa;tion,  ranging  from  primary  [6],  secondary  [19],  higher  education  [7],  [14],  and  for &#xd;&#xa;children with special educational needs [35]." ID="ID_427692682" CREATED="1389284911585" MODIFIED="1389284911585" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="56" object_id="3601264766204391782" object_number="959" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Serious Games are games that &#xd;&#xa;educate, train and inform using entertainment principles, creativity,  and tech-&#xd;&#xa;nology." ID="ID_598030090" CREATED="1389284911607" MODIFIED="1389284911607" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="84" object_id="573358386031117641" object_number="1096" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" A serious  game is a &#xd;&#xa;game designed for a primary purpose other than pure entertainment. Serious games &#xd;&#xa;can  be  applied  to  a  broad  spectrum  of  application  areas,  e.g.  military,  government, &#xd;&#xa;educational, corporate, and healthcare." ID="ID_1674879788" CREATED="1389284911629" MODIFIED="1389284911629" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="84" object_id="7794920846928164575" object_number="1097" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" It is the addition of pedagogy (activities that educate &#xd;&#xa;or  instruct,  thereby  imparting  knowledge  or  skill)  that  makes  games  serious." ID="ID_1837503453" CREATED="1389284911651" MODIFIED="1389284911651" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="84" object_id="5802563777661345267" object_number="1098" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="4.1  Serious Games Awareness &#xd;&#xa;According  to  our  survey  almost  29%  of  companies  responded  that  they  have   &#xd;&#xa;heard about the use of Serious Games for corporate training. However, 15 out of 21 " ID="ID_79428574" CREATED="1389284911673" MODIFIED="1389284911673" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="88" object_id="1532720562655866818" object_number="1127" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="serious game vs teacher" ID="ID_636268637" CREATED="1389284911694" MODIFIED="1389284911694" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="COMMENT" page="124" object_id="1615764751321596691" object_number="4866" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Opinions on Bringing Serious Games into the Classroom. " ID="ID_1543317068" CREATED="1389284911714" MODIFIED="1389284911714" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Serious%20Games%20Development%20(sg%20notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="127" object_id="5924916469437879472" object_number="4876" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="speaker6-sawyer-final.ppt" ID="ID_1880037080" CREATED="1389352197923" MODIFIED="1389352197923" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/speaker6-sawyer-final.ppt"/>
<node TEXT="classifying_serious_games.pdf" ID="ID_1348989609" CREATED="1389352202314" MODIFIED="1389352202314" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/classifying_serious_games.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="6FEABCC28A8B250E0A13152B5BC8330BF328CC1E8BAF3EC902392C51D00">
    <pdf_title>Classifying Serious Games: the G/P/S model</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Sawyer_Smith_SeriousGamesTaxonomy_2008.pdf" ID="ID_1384254384" CREATED="1389368706409" MODIFIED="1389368706410" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Sawyer_Smith_SeriousGamesTaxonomy_2008.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="1299D046F938C6BCC4B96E7D355166EE4138773A974CBE1C56D2C64624526AD">
    <pdf_title>oo</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Sawyer_TheSeriousGamesLandscape_2007.ppt" ID="ID_547523462" CREATED="1389368706466" MODIFIED="1389368706466" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Sawyer_TheSeriousGamesLandscape_2007.ppt"/>
<node TEXT="Breuer_Bente_WhySoSerious.pdf" ID="ID_13652753" CREATED="1389368706539" MODIFIED="1389368706539" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
<node TEXT="Game designers &#xd;&#xa;Michael and Chen (2006) e.g. define serious games as follows: &#x201c;A serious game is a &#xd;&#xa;game  in  which  education  (in  its  various  forms)  is  the  primary  goal,  rather  than &#xd;&#xa;entertainment&#x201d; (Michael and Chen 2006, p.17). " ID="ID_1584560655" CREATED="1389368706734" MODIFIED="1389368706734" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="3" object_id="526584784236050606" object_number="32" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" &#x201c;Serious games have more than just story, art, and &#xd;&#xa;software,  however.  (&#x2026;)  They  involve  pedagogy:  activities  that  educate  or  instruct, &#xd;&#xa;thereby  imparting  knowledge  or  skill.  This  addition  makes  games  serious&#x201d;  (Zyda &#xd;&#xa;2005,  p.26). " ID="ID_1748545592" CREATED="1389368706669" MODIFIED="1389368706669" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="3" object_id="5911839358564546130" object_number="33" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" &#x201c;We  are  concerned  with  serious  games  in  the  sense  that  these  games &#xd;&#xa;have an explicit and carefully thought-out educational purpose and are not intended &#xd;&#xa;to be played primarily for amusement.&#x201d; (Abt 1975, p.9)." ID="ID_178206910" CREATED="1389368706603" MODIFIED="1389368706604" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="3" object_id="6939102611248820198" object_number="34" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Since  the  aim  of  blending &#xd;&#xa;entertainment and education or fun and learning is also the driving force behind the &#xd;&#xa;serious  games  movement,  one  could  ask  whether  serious  games  are  just  a  new &#xd;&#xa;branch of edutainment. Michael and Chen (2006) disagree and postulate that serious &#xd;&#xa;games  &#x201c;are  more  than  just  &#x2018;edutainment&#x2019;&#x201d;  (Michael  and  Chen  2006,  p.  XV).  " ID="ID_444286914" CREATED="1389368706517" MODIFIED="1389368706517" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="4" object_id="1973161557455315201" object_number="37" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT=" Despite  the  increased  attention  and &#xd;&#xa;importance which serious games have been receiving lately, the definition of the term &#xd;&#xa;&#x2018;serious game&#x2019; often varies depending on who uses it and in what context." ID="ID_915247504" CREATED="1389782673395" MODIFIED="1389782673395" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="2" object_id="554595744011558673" object_number="30" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The term &#x2018;serious game&#x2019; as it applies to digital games was coined &#xd;&#xa;by Ben Sawyer in his 2003 paper on the potential of using digital games for policy &#xd;&#xa;making (Sawyer 2003). " ID="ID_487900447" CREATED="1389782673445" MODIFIED="1389782673445" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="3" object_id="1635951635036815468" object_number="36" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Figure  1 &#xd;&#xa;summarizes  the  relations  of  edutainment,  entertainment  education,  (digital)  game-&#xd;&#xa;based learning, e-learning and serious games. " ID="ID_1170974324" CREATED="1389782673498" MODIFIED="1389782673499" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="5" object_id="3161047024068963286" object_number="42" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Figure 1: The relations between serious games and similar educational concepts " ID="ID_676557639" CREATED="1389782673546" MODIFIED="1389782673546" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Breuer_Bente_WhySoSerious.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="6" object_id="1798890866938546328" object_number="45" document_hash="9FF9FD471A27182C45624CE231D4FCFFF25FD8B74EA7D4DC1B38E7949F9">
    <pdf_title>http://www.eludamos.org</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="Sawyer_Rejeski_SeriousGames_improving public policy though game-based learning and simulation.pdf" ID="ID_1142452315" CREATED="1389368706802" MODIFIED="1389368706802" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Sawyer_Rejeski_SeriousGames_improving%20public%20policy%20though%20game-based%20learning%20and%20simulation.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="2197F0DE509EB937D064967A0C45B6F1DE22EB3BD1C6064436E54D4762A70AA">
    <pdf_title>Serious Games: Serious Games: Serious Games: Serious Games:</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Sawyer_TheSeriousGamesLandscape_2007.pdf" ID="ID_19831515" CREATED="1389782666380" MODIFIED="1389782666380" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Sawyer_TheSeriousGamesLandscape_2007.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="CD15CC938CA442757F8D71CE597E8C2F8FB266165391C4D1D8E9E1F4C">
    <pdf_title>Ben Sawyer</pdf_title>
</pdf_annotation>
</node>
<node TEXT="godforintro.pdf" ID="ID_315122555" CREATED="1389782666492" MODIFIED="1389782666492" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/godforintro.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="D191CE69216425BD1B6885E5726BDB2506250555186DEA2BDECEFAC19AF128">
    <pdf_title>Serious games</pdf_title>
</pdf_annotation>
<node TEXT="good for intro" ID="ID_584732359" CREATED="1389782670786" MODIFIED="1389782670786" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/godforintro.pdf">
<pdf_annotation type="COMMENT" page="1" object_id="1599566393959561424" object_number="111" document_hash="D191CE69216425BD1B6885E5726BDB2506250555186DEA2BDECEFAC19AF128">
    <pdf_title>Serious games</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="ESA_EF_2013.pdf" ID="ID_748256659" CREATED="1389782673333" MODIFIED="1389782673333" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/ESA_EF_2013.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="43921F35F35D83C826A12A7F6DFE0DD67C134B895F2A5EA079B8947C6287">
    <pdf_title>ESSENTIAL FACTS</pdf_title>
</pdf_annotation>
</node>
<node TEXT="ESA_EF_2011.pdf" ID="ID_406070382" CREATED="1389782673364" MODIFIED="1389782673364" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/ESA_EF_2011.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="1A277D9A5484D918BD206D53B3EDDEDC5AA856501022FB4847838DD5FC5269">
    <pdf_title>ESSENTIAL FACTS</pdf_title>
</pdf_annotation>
</node>
<node TEXT="AmbientInsight_SeriousPlay2013-2017_WW_GameBasedLearning_Market.pdf" ID="ID_1117905893" CREATED="1389782673594" MODIFIED="1389782673594" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/AmbientInsight_SeriousPlay2013-2017_WW_GameBasedLearning_Market.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="6F9DF24F5C8054A37D1E8B065719486403C7CDFD9D2AAC1A5C16E33B97216BF">
    <pdf_title>The 2012-2017 Worldwide Game-based Learning and Simulation-based Markets</pdf_title>
</pdf_annotation>
</node>
<node TEXT="PIP_Teens_Games_and_Civics_Report_FINAL.pdf" ID="ID_1826002471" CREATED="1389782673663" MODIFIED="1389782673663" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
<node TEXT="Almost all teens play games. " ID="ID_48349412" CREATED="1389782673838" MODIFIED="1389782673839" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="2" object_id="7846558049603503943" object_number="420" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Gender and age are key factors in describing teens&#x2019; video gaming. " ID="ID_1218429627" CREATED="1389782673887" MODIFIED="1389782673887" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="2" object_id="8480920110958776722" object_number="576" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Youth play many different kinds of video games." ID="ID_692415244" CREATED="1389782673934" MODIFIED="1389782673934" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="3" object_id="223866903850873742" object_number="574" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Gaming is often a social experience for teens. " ID="ID_232865098" CREATED="1389782673984" MODIFIED="1389782673984" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="4" object_id="6623511410988087348" object_number="572" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Close to half of teens who play online games do so with people they know in their offline lives." ID="ID_294350658" CREATED="1389782674033" MODIFIED="1389782674033" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="5" object_id="4913622478780031562" object_number="570" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Teens encounter both pro-social and anti-social behavior while gaming. " ID="ID_822970436" CREATED="1389782674084" MODIFIED="1389782674084" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="5" object_id="7991204332417286739" object_number="568" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The most popular game genres include games with violent and nonviolent content." ID="ID_901874589" CREATED="1389782674134" MODIFIED="1389782674134" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="5" object_id="6592950564019565383" object_number="566" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Parental monitoring of game play varies. " ID="ID_1258427133" CREATED="1389782674183" MODIFIED="1389782674183" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="6" object_id="1762762490123954914" object_number="564" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="There are civic dimensions to video game play." ID="ID_146667802" CREATED="1389782674231" MODIFIED="1389782674232" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="6" object_id="4722398350723202" object_number="562" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The quantity of game play is not strongly related to teens&#x2019; interest or engagement in civic and political activity. " ID="ID_1294869313" CREATED="1389782674279" MODIFIED="1389782674280" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="7" object_id="7783146933916272801" object_number="560" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The characteristics of game play and the contexts in which teens play games are strongly related to teens&#x2019; interest and engagement in civic and political activities. " ID="ID_738098002" CREATED="1389782674329" MODIFIED="1389782674330" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="7" object_id="7370502478483915564" object_number="558" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Playing games with others in person was related to civic and political outcomes, but playing with others online was not. " ID="ID_666402588" CREATED="1389782674378" MODIFIED="1389782674378" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="7" object_id="4615070467521701876" object_number="556" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Teens who take part in social interaction related to the game, such as commenting on websites or contributing to discussion boards, are more engaged civically and politically. " ID="ID_566631744" CREATED="1389782674428" MODIFIED="1389782674428" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="8" object_id="6226744378386157261" object_number="554" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Civic gaming experiences are more equally distributed than many other civic learning opportunities. " ID="ID_1852431347" CREATED="1389782674478" MODIFIED="1389782674478" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="9" object_id="2147167166756286416" object_number="552" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Moving beyond the polarized video game debate reveals a variety of gaming experiences and contexts." ID="ID_1478442829" CREATED="1389782674545" MODIFIED="1389782674545" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="14" object_id="3817121875222936709" object_number="548" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
<node TEXT="Educational games" ID="ID_356218826" CREATED="1389782674594" MODIFIED="1389782674594" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="15" object_id="7033622931242148814" object_number="549" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Civics and Game Play" ID="ID_1084002438" CREATED="1389782674664" MODIFIED="1389782674664" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="16" object_id="380818319031865454" object_number="579" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Gaming and Civics Research Topics" ID="ID_1515113913" CREATED="1389782674730" MODIFIED="1389782674730" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="17" object_id="2710013684602713442" object_number="551" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="Virtually all teens play games." ID="ID_1454473532" CREATED="1389782674798" MODIFIED="1389782674798" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="20" object_id="1937329279933629214" object_number="546" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Half of teens who play games do so on any given day." ID="ID_1364732339" CREATED="1389782674846" MODIFIED="1389782674846" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="20" object_id="3060714608804574757" object_number="544" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Almost all girls and boys play video games. Boys report playing games more often and for longer periods of time than girls." ID="ID_1427178760" CREATED="1389782674896" MODIFIED="1389782674896" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="21" object_id="6275228558307039561" object_number="542" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Younger teens are the most avid gamers." ID="ID_305056836" CREATED="1389782674946" MODIFIED="1389782674946" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="22" object_id="249584665170337102" object_number="540" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Broadband users are slightly more likely to play for longer periods of time than teens who reside in homes without broadband. " ID="ID_1084019532" CREATED="1389782674994" MODIFIED="1389782674994" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="22" object_id="639144869938647939" object_number="538" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The daily gamer: Young, male, and playing games online" ID="ID_1550509827" CREATED="1389782675043" MODIFIED="1389782675043" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="22" object_id="618407284539535935" object_number="536" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Frequent game players are not socially isolated. " ID="ID_685595503" CREATED="1389782675091" MODIFIED="1389782675092" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="23" object_id="973157188143093794" object_number="534" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="What devices do teens use to play games?" ID="ID_242086074" CREATED="1389782675141" MODIFIED="1389782675141" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="24" object_id="7784635999697669691" object_number="532" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Consoles are the most common way to play games." ID="ID_31296304" CREATED="1389782675189" MODIFIED="1389782675189" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="24" object_id="3471321521014626626" object_number="530" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Personal computers are used equally by all groups for game play." ID="ID_282869116" CREATED="1389782675240" MODIFIED="1389782675240" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="25" object_id="1145074463226136374" object_number="528" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Younger teens are the most likely to play on portable gaming devices." ID="ID_777806530" CREATED="1389782675288" MODIFIED="1389782675288" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="26" object_id="8162502554108319970" object_number="526" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Girls and black and lower-income teens are more likely to use cell phones to play games." ID="ID_94617214" CREATED="1389782675337" MODIFIED="1389782675337" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="26" object_id="2481949420454639045" object_number="524" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Most teens own multiple gaming devices. " ID="ID_183081846" CREATED="1389782675386" MODIFIED="1389782675386" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="27" object_id="5726354044854471979" object_number="522" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Teens play many different types of games. " ID="ID_1902424008" CREATED="1389782675438" MODIFIED="1389782675438" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="28" object_id="8657876629033756725" object_number="520" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Most teens play a variety of different game genres. " ID="ID_995550203" CREATED="1389782675487" MODIFIED="1389782675488" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="29" object_id="830948651068442321" object_number="518" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="There are differences by race/ethnicity in the types of games played." ID="ID_1438657995" CREATED="1389782675536" MODIFIED="1389782675536" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="29" object_id="2983478168943162551" object_number="516" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Lower-income teens more likely to play certain game genres." ID="ID_1499590714" CREATED="1389782675584" MODIFIED="1389782675584" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="29" object_id="2129064110270684050" object_number="514" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Boys are more likely to play most game genres." ID="ID_99686721" CREATED="1389782675634" MODIFIED="1389782675634" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="29" object_id="1864019616879312056" object_number="512" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Frequent gamers play a similar range of game genres as boys." ID="ID_1914804999" CREATED="1389782675681" MODIFIED="1389782675682" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="31" object_id="582030484715747036" object_number="510" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Younger teens are more likely to leap into virtual worlds. " ID="ID_165127419" CREATED="1389782675728" MODIFIED="1389782675728" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="31" object_id="3084822895026737999" object_number="508" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="What are massive multiplayer online games?" ID="ID_1418544220" CREATED="1389782675779" MODIFIED="1389782675779" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="32" object_id="5323594429060633227" object_number="506" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Three in ten gaming boys play MMOGs." ID="ID_375029225" CREATED="1389782675828" MODIFIED="1389782675828" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="32" object_id="8569000360150751440" object_number="504" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="What are the most popular game titles?" ID="ID_1302137054" CREATED="1389782675876" MODIFIED="1389782675876" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="32" object_id="7971118840131500496" object_number="502" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Profiles of Top Game Franchises" ID="ID_318080394" CREATED="1389782675924" MODIFIED="1389782675924" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="34" object_id="6095732301533068670" object_number="498" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
<node TEXT="Guitar Hero" ID="ID_385957299" CREATED="1389782675973" MODIFIED="1389782675973" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="34" object_id="276616724521310253" object_number="499" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Halo" ID="ID_1924666367" CREATED="1389782676051" MODIFIED="1389782676051" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="34" object_id="1507249331568590173" object_number="587" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Madden" ID="ID_950188569" CREATED="1389782676139" MODIFIED="1389782676139" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="34" object_id="4866110511434692252" object_number="585" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The Sims" ID="ID_1102277662" CREATED="1389782676222" MODIFIED="1389782676222" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="35" object_id="2104926406930974500" object_number="583" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Grand Theft Auto" ID="ID_399281581" CREATED="1389782676302" MODIFIED="1389782676302" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="35" object_id="7349487255078660304" object_number="501" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="The average rating for teens&#x2019; favorite games is just above a Teen rating. " ID="ID_981974877" CREATED="1389782676397" MODIFIED="1389782676397" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="35" object_id="4396836624359907409" object_number="496" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Nearly one-third of young teens play M- or AO-rated games." ID="ID_1823102364" CREATED="1389782676461" MODIFIED="1389782676461" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="37" object_id="3990123995143900307" object_number="494" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Games are social experiences for the majority of teens. " ID="ID_297807493" CREATED="1389782676517" MODIFIED="1389782676517" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="38" object_id="7622140868088281175" object_number="492" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Many teens play games with others. " ID="ID_141763276" CREATED="1389782676561" MODIFIED="1389782676561" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="38" object_id="256162742020906318" object_number="490" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Teens also play games alone." ID="ID_186907222" CREATED="1389782676616" MODIFIED="1389782676616" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="39" object_id="1621163771874508019" object_number="488" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="More than half of teens most often play with other people." ID="ID_511014430" CREATED="1389782676662" MODIFIED="1389782676662" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="39" object_id="6276962192522116519" object_number="486" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="A significant number of online gamers play games in groups." ID="ID_1579674741" CREATED="1389782676713" MODIFIED="1389782676713" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="39" object_id="7748034438297772444" object_number="484" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Most teens play online games with friends they know in their offline lives." ID="ID_1916950162" CREATED="1389782676766" MODIFIED="1389782676766" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="40" object_id="3070630004721498437" object_number="482" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="MMOG players are much more likely to play games with others online." ID="ID_1710861081" CREATED="1389782676823" MODIFIED="1389782676823" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="40" object_id="509473067390364697" object_number="480" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="More than one in three teens has played a video game for school. " ID="ID_1544336553" CREATED="1389782676869" MODIFIED="1389782676869" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="41" object_id="3272185454020698287" object_number="478" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Pro-social and anti-social behavior in games" ID="ID_1736945921" CREATED="1389782676924" MODIFIED="1389782676924" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="41" object_id="4165854206166251564" object_number="476" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Both violent and nonviolent games were among the most popular franchises reported in teens&#x2019; top three games. " ID="ID_1852905233" CREATED="1389782676979" MODIFIED="1389782676979" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="42" object_id="3927882314021208402" object_number="474" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Despite efforts to limit teens&#x2019; access to ultra-violent or sexually explicit games, many teens report playing mature- and adult-only-rated games. " ID="ID_1925435255" CREATED="1389782677029" MODIFIED="1389782677029" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="42" object_id="5585750066835620972" object_number="472" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The majority of teens who play games encounter aggressive behavior while playing games, and most of those teens witness others stepping in to stop the behavior. " ID="ID_1867519543" CREATED="1389782677076" MODIFIED="1389782677076" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="43" object_id="8663229610258930593" object_number="470" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="For many, it is more than game play&#x2014;36% of gamers read game-related reviews, websites, and discussions." ID="ID_603748647" CREATED="1389782677125" MODIFIED="1389782677125" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="44" object_id="3575080411799779888" object_number="468" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Boys and younger teens are more likely read game-related websites and discussions. " ID="ID_426041102" CREATED="1389782677174" MODIFIED="1389782677174" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="45" object_id="737126052765538647" object_number="466" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Playing games with others online, playing MMOGs and virtual worlds all increase the likelihood that a gamer will visit game-related websites. " ID="ID_1033298626" CREATED="1389782677245" MODIFIED="1389782677245" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="45" object_id="7555066496325010087" object_number="464" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="More than one in ten teens contribute to game-related websites." ID="ID_1857190117" CREATED="1389782677293" MODIFIED="1389782677294" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="46" object_id="8959821475032819160" object_number="462" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="More than one-third of teen gamers use cheat codes or game hacks." ID="ID_1082839429" CREATED="1389782677339" MODIFIED="1389782677339" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="46" object_id="8862248289254284037" object_number="460" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Close to three in ten gaming teens have used &#x201c;mods&#x201d; to alter the games they play." ID="ID_558464977" CREATED="1389782677388" MODIFIED="1389782677388" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="46" object_id="8890983685204110005" object_number="458" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Teens who use game cheats and mods are more likely to visit game-related websites." ID="ID_590109398" CREATED="1389782677433" MODIFIED="1389782677433" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="47" object_id="1138548663208884696" object_number="456" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="A majority of parents are aware that their children play video games." ID="ID_85810476" CREATED="1389782677482" MODIFIED="1389782677482" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="48" object_id="1965639239668093518" object_number="454" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Parents know what games their children play&#x2014;at least some of the time." ID="ID_1134991244" CREATED="1389782677529" MODIFIED="1389782677529" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="48" object_id="4660680626386761593" object_number="452" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="More than half of parents report &#x201c;always&#x201d; checking video game ratings." ID="ID_1927143713" CREATED="1389782677577" MODIFIED="1389782677578" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="49" object_id="1944148992225284754" object_number="450" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Parents of boys are more likely to intervene in game play than parents of girls." ID="ID_1755182247" CREATED="1389782677624" MODIFIED="1389782677624" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="49" object_id="4768306078372973469" object_number="448" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Few parents play games with their children." ID="ID_915126157" CREATED="1389782677684" MODIFIED="1389782677684" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="49" object_id="2199787920717455842" object_number="446" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Parents are unlikely to emphasize the impact of video games on their own children." ID="ID_829057893" CREATED="1389782677729" MODIFIED="1389782677729" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="50" object_id="2710819334067507723" object_number="444" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Are there connections between games and civic life?" ID="ID_1575485718" CREATED="1389782677777" MODIFIED="1389782677777" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="52" object_id="7040413862422796308" object_number="442" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Some civic gaming experiences are more common than others. " ID="ID_1058947466" CREATED="1389782677822" MODIFIED="1389782677822" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="54" object_id="6271584811315046504" object_number="440" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Teens have varying levels of civic gaming experiences. " ID="ID_1063257122" CREATED="1389782677870" MODIFIED="1389782677870" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="54" object_id="3296276087127317908" object_number="438" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Roughly 60% of teens are interested in politics, charitable work, and express a sense of commitment to civic participation." ID="ID_648428639" CREATED="1389782677915" MODIFIED="1389782677915" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="54" object_id="6321103894411056942" object_number="436" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The quantity of game play is not strongly related (positively or negatively) to most indicators of teens&#x2019; interest and engagement in civic and political activity. " ID="ID_1068773121" CREATED="1389782677964" MODIFIED="1389782677964" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="55" object_id="5927646608201591460" object_number="434" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The characteristics of game play are strongly related to teens&#x2019; interest and engagement in civic and political activity. " ID="ID_917150148" CREATED="1389782678013" MODIFIED="1389782678013" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="55" object_id="6380396707203258299" object_number="432" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Playing games with others in person is related to civic and political outcomes, but playing with others online is not. " ID="ID_1480827504" CREATED="1389782678057" MODIFIED="1389782678058" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="57" object_id="2026290182264797379" object_number="430" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Youth who take part in social interaction related to the game, such as  commenting on websites or contributing to discussion boards, are more engaged civically and politically. Youth who play games where they are part of guilds are not more civically engaged than youth who play games alone." ID="ID_683571319" CREATED="1389782678106" MODIFIED="1389782678106" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="58" object_id="5115671689120278918" object_number="428" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Civic gaming opportunities appear to be more equitably distributed than high school civic learning opportunities. " ID="ID_1062260281" CREATED="1389782678152" MODIFIED="1389782678152" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="59" object_id="2644984812154202156" object_number="425" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
<node TEXT="Regression Results" ID="ID_1217750961" CREATED="1389782678200" MODIFIED="1389782678200" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="63" object_id="9201947933920561260" object_number="426" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="Parent and Teen Survey on Gaming and Civic Engagement" ID="ID_1130022864" CREATED="1389782673644" MODIFIED="1389782673644" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="69" object_id="2580391277891631989" object_number="421" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
<node TEXT="Summary" ID="ID_1261764865" CREATED="1389782678262" MODIFIED="1389782678262" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="69" object_id="259438676416263721" object_number="422" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Design and Data Collection Procedures" ID="ID_416395848" CREATED="1389782678325" MODIFIED="1389782678325" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="69" object_id="180923918108437336" object_number="602" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Response Rate" ID="ID_1219095839" CREATED="1389782678388" MODIFIED="1389782678388" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="72" object_id="4034248548315293922" object_number="600" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Open-Ended Response Coding Instructions" ID="ID_631892200" CREATED="1389782678452" MODIFIED="1389782678452" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="74" object_id="4964871062836212879" object_number="598" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Coding Game Ratings" ID="ID_400211045" CREATED="1389782678516" MODIFIED="1389782678516" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="74" object_id="1525100729063280862" object_number="596" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Coding the Individual Respondent Game Ratings" ID="ID_1477873020" CREATED="1389782673776" MODIFIED="1389782673776" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="75" object_id="4874890140307775960" object_number="594" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Coding for M- and/or AO-Rated Games" ID="ID_217765824" CREATED="1389782673712" MODIFIED="1389782673712" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="75" object_id="4357426228513261397" object_number="592" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Coding for Top Five Game Franchises" ID="ID_1140437127" CREATED="1389782673624" MODIFIED="1389782673624" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/PIP_Teens_Games_and_Civics_Report_FINAL.pdf">
<pdf_annotation type="BOOKMARK" page="76" object_id="5767006714332902424" object_number="424" document_hash="F5F33A9A4624FEE9528EA6D2F2C1378996BC3B5788F62FE2843BC69DA2BD7CBF">
    <pdf_title>Teens, Video Games, and Civics</pdf_title>
</pdf_annotation>
</node>
</node>
</node>
<node TEXT="Niroshan_Thillainathan_AModelDrivenDevelopmentFrameworkForSeriousGames.pdf" ID="ID_1990302443" CREATED="1389782678578" MODIFIED="1389782678578" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Serious%20Games/Niroshan_Thillainathan_AModelDrivenDevelopmentFrameworkForSeriousGames.pdf">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="A2F38231F86FA3B36392DBA97B0F5D17369941A7A1EEE2BD4290C5D853F546">
    <pdf_title>Please quote as: Thillainathan, N. (2013): A Model Driven Development Framework for Serious Games. In: Informatik 2013 Doctoral Consortium, Koblenz, Germany.</pdf_title>
</pdf_annotation>
</node>
</node>
</node>
<node TEXT="Game Engines" POSITION="left" ID="ID_229773896" CREATED="1389352479652" MODIFIED="1389352510408">
<edge COLOR="#0000ff"/>
<attribute NAME="mon_incoming_folder" VALUE="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/" OBJECT="java.net.URI|project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/"/>
<attribute NAME="mon_mindmap_folder" VALUE="@@library_mindmaps@@"/>
<attribute NAME="mon_auto" VALUE="2" OBJECT="java.lang.Integer|2"/>
<attribute NAME="mon_subdirs" VALUE="2" OBJECT="java.lang.Integer|2"/>
<attribute NAME="mon_add_extra_incoming_node" VALUE="2" OBJECT="java.lang.Integer|2"/>
<attribute NAME="mon_flatten_dirs" VALUE="2" OBJECT="java.lang.Integer|2"/>
<node TEXT="Incoming" ID="ID_915733824" CREATED="1389352511760" MODIFIED="1389352511868" INCOMING="true" COLOR="#ffffff" BACKGROUND_COLOR="#006699">
<font NAME="Courier New" BOLD="true"/>
</node>
<node TEXT="UDK" ID="ID_197552482" CREATED="1389794587443" MODIFIED="1389794593773"/>
<node TEXT="Unity" ID="ID_1445249459" CREATED="1389794599419" MODIFIED="1389794601580"/>
<node TEXT="General" ID="ID_44226181" CREATED="1389794606956" MODIFIED="1389794609389">
<node TEXT="Serious Games Development (GE Notes).pdf" ID="ID_1211633265" CREATED="1389352511938" MODIFIED="1389352511938" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf" MOVED="1389794612458">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
<node TEXT="5  Software Selection " ID="ID_1848052083" CREATED="1389352512458" MODIFIED="1389352512458" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="60" object_id="3004787081418971466" object_number="980" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="The rise in popularity of computer games over the past decade &#xd;&#xa;has caused a  wide range of  game engines to enter the  market in order to support &#xd;&#xa;their development. The role of modern game engines is to streamline and simplify &#xd;&#xa;the process of game development by providing the developer with common func-&#xd;&#xa;tionality and visual development tools from which to build an application." ID="ID_1142497549" CREATED="1389352512399" MODIFIED="1389352512399" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="61" object_id="5468447838647114779" object_number="991" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="aspects to compare:&#xa;- features and functionality&#xa;- gui of thse framework&#xa;- model loading&#xa;- gui creation&#xa;- programming languages&#xa;- interaktionsm&#xf6;glichkeiten&#xa;- deployment platforms" ID="ID_698897905" CREATED="1389352512341" MODIFIED="1389352512341" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="COMMENT" page="61" object_id="535108742946083013" object_number="992" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
<node TEXT="what are our requirements to the game engine?&#xa;- import von g&#xe4;ngigen 3d model formaten&#xa;- umfangreiche gui erstellungsm&#xf6;glichkeiten" ID="ID_1697991317" CREATED="1389352512103" MODIFIED="1389352537790" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf" MOVED="1389782232718">
<pdf_annotation type="COMMENT" page="61" object_id="5147830511164746873" object_number="997" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
<node TEXT="" ID="ID_1049852650" CREATED="1389794571774" MODIFIED="1389794571774"/>
</node>
</node>
<node TEXT=" Given  these  broad  requirements  the  following  game  engines  were  &#xd;&#xa;highlighted for further consideration. " ID="ID_1600615750" CREATED="1389352512284" MODIFIED="1389782257485" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="61" object_id="7005170146931316676" object_number="994" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="5.1  Unreal Development Kit " ID="ID_1283915598" CREATED="1389352512227" MODIFIED="1389352512227" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="61" object_id="6186287207930375394" object_number="995" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="5.2  Quest 3D " ID="ID_1049628424" CREATED="1389352512158" MODIFIED="1389352512158" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="61" object_id="7095608290306350805" object_number="996" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="5.3  Unity " ID="ID_1558673998" CREATED="1389352512048" MODIFIED="1389352512048" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="62" object_id="5710548494006305033" object_number="1001" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="5.4  Evaluation" ID="ID_1169899398" CREATED="1389352511991" MODIFIED="1389352511991" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="62" object_id="5607649559888593251" object_number="1002" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
<node TEXT="5.5  Other Game Development Tools" ID="ID_1428313761" CREATED="1389352511914" MODIFIED="1389352511914" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/Serious%20Games%20Development%20(GE%20Notes).pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="62" object_id="2432109864202638839" object_number="1003" document_hash="CE492CF6DEB5867156218B693E7661E7876423B693E1DEE3CF19276BBA8C1C">
    <pdf_title>Lecture Notes in Computer Science 7528</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="game engines selection framework for high-fidelity serious applications.pdf" ID="ID_1224164078" CREATED="1389794623064" MODIFIED="1389794623064" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/game%20engines%20selection%20framework%20for%20high-fidelity%20serious%20applications.pdf" MOVED="1389794635711">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="7B3843AB3AC5F01147A548B5F4B1B168EC9095E41A67678DAE55DC668D1AD6">
    <pdf_title>Game Engines Selection Framework for High-Fidelity Serious Applications</pdf_title>
</pdf_annotation>
<node TEXT="Summary of Requirements for Serious &#xd;&#xa;Games " ID="ID_7762305" CREATED="1389794623237" MODIFIED="1389794623237" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/game%20engines%20selection%20framework%20for%20high-fidelity%20serious%20applications.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="6" object_id="8765152216810547868" object_number="34" document_hash="7B3843AB3AC5F01147A548B5F4B1B168EC9095E41A67678DAE55DC668D1AD6">
    <pdf_title>Game Engines Selection Framework for High-Fidelity Serious Applications</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Overview of Modern Game Engines" ID="ID_175911274" CREATED="1389794623178" MODIFIED="1389794623178" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/game%20engines%20selection%20framework%20for%20high-fidelity%20serious%20applications.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="6" object_id="2134259658928699921" object_number="35" document_hash="7B3843AB3AC5F01147A548B5F4B1B168EC9095E41A67678DAE55DC668D1AD6">
    <pdf_title>Game Engines Selection Framework for High-Fidelity Serious Applications</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Table 1: Framework for Comparing Engines in SG " ID="ID_261080908" CREATED="1389794623119" MODIFIED="1389794623119" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/game%20engines%20selection%20framework%20for%20high-fidelity%20serious%20applications.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="8" object_id="3127847687614254458" object_number="40" document_hash="7B3843AB3AC5F01147A548B5F4B1B168EC9095E41A67678DAE55DC668D1AD6">
    <pdf_title>Game Engines Selection Framework for High-Fidelity Serious Applications</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Current Game Engines Compared Using the &#xd;&#xa;Framework " ID="ID_1560863515" CREATED="1389794623038" MODIFIED="1389794623038" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/game%20engines%20selection%20framework%20for%20high-fidelity%20serious%20applications.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="8" object_id="3322193480577262237" object_number="41" document_hash="7B3843AB3AC5F01147A548B5F4B1B168EC9095E41A67678DAE55DC668D1AD6">
    <pdf_title>Game Engines Selection Framework for High-Fidelity Serious Applications</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="3d_engines_in_games.pdf" ID="ID_219529403" CREATED="1389794622980" MODIFIED="1389794622980" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/3d_engines_in_games.pdf" MOVED="1389794640508">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="35791CA09AE2B139BFE12E1FD9BEE84165EA6321A2A153DDE9182E7B06C8BAA">
    <pdf_title>3D Engines in games - Introduction</pdf_title>
</pdf_annotation>
<node TEXT="game engine scheme&#xd;&#xa;figure" ID="ID_1539695145" CREATED="1389794622959" MODIFIED="1389794622961" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/3d_engines_in_games.pdf">
<pdf_annotation type="COMMENT" page="9" object_id="4697251860235982945" object_number="45" document_hash="35791CA09AE2B139BFE12E1FD9BEE84165EA6321A2A153DDE9182E7B06C8BAA">
    <pdf_title>3D Engines in games - Introduction</pdf_title>
</pdf_annotation>
</node>
</node>
<node TEXT="vergleich von 3d game engines - unreal3 vs irrlicht.pdf" ID="ID_1246501809" CREATED="1389807954309" MODIFIED="1389807954309" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/vergleich%20von%203d%20game%20engines%20-%20unreal3%20vs%20irrlicht.pdf" MOVED="1389807973575">
<pdf_annotation type="PDF_FILE" object_id="0" document_hash="712DC5E1426993FDD9C6FF272E88F2D444FCE6492F125A394271F6BAB8D4AF38">
    <pdf_title>Vergleich von 3D Game Engines</pdf_title>
</pdf_annotation>
<node TEXT="Eine Spiel-Engine ist ein Computerprogramm, das den &#xd;&#xa;Spielverlauf steuert und f&#xfc;r die visuelle Darstellung des &#xd;&#xa;Spieleablaufs verantwortlich ist. " ID="ID_904830313" CREATED="1389807954449" MODIFIED="1389807954449" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/vergleich%20von%203d%20game%20engines%20-%20unreal3%20vs%20irrlicht.pdf">
<pdf_annotation type="HIGHLIGHTED_TEXT" page="7" object_id="6519469540205623471" object_number="129" document_hash="712DC5E1426993FDD9C6FF272E88F2D444FCE6492F125A394271F6BAB8D4AF38">
    <pdf_title>Vergleich von 3D Game Engines</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Warren Schultz (http://gameindustry.about.com/od/resources/g/Game-Engine.htm)&#xd;&#xa;&#xd;&#xa;A core set of technologies combined into a single software package to accelerate game development. A full game engine typically includes a graphics renderer, a network layer, a sound engine, a physics system, a scripting language/interpreter, and a graphical previewer/editor. Some more advanced engines will incorporate shader editors, terrain systems, and other, more advanced, features." ID="ID_1260503968" CREATED="1389807954418" MODIFIED="1389807954418" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/vergleich%20von%203d%20game%20engines%20-%20unreal3%20vs%20irrlicht.pdf">
<pdf_annotation type="COMMENT" page="7" object_id="8012083599459045474" object_number="130" document_hash="712DC5E1426993FDD9C6FF272E88F2D444FCE6492F125A394271F6BAB8D4AF38">
    <pdf_title>Vergleich von 3D Game Engines</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Wikipedia (http://en.wikipedia.org/wiki/Game_engine):&#xd;&#xa;&#xd;&#xa;A game engine is a system designed for the creation and development of video games. The leading game engines provide a software framework that developers use to create games for video game consoles, mobile devices and personal computers. The core functionality typically provided by a game engine includes a rendering engine (&#x201c;renderer&#x201d;) for 2D or 3D graphics, a physics engine or collision detection (and collision response), sound, scripting, animation, artificial intelligence, networking, streaming, memory management, threading, localization support, and a scene graph. The process of game development is often economized, in large part, by reusing/adapting the same game engine to create different games,[1] or to make it easier to &quot;port&quot; games to multiple platforms." ID="ID_1600231605" CREATED="1389807954402" MODIFIED="1389807954402" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/vergleich%20von%203d%20game%20engines%20-%20unreal3%20vs%20irrlicht.pdf">
<pdf_annotation type="COMMENT" page="7" object_id="6525884620042554594" object_number="132" document_hash="712DC5E1426993FDD9C6FF272E88F2D444FCE6492F125A394271F6BAB8D4AF38">
    <pdf_title>Vergleich von 3D Game Engines</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Jeff Ward (http://www.gamecareerguide.com/features/529/what_is_a_game.php)&#xd;&#xa;&#xd;&#xa;After all, the game engine, much like a car&apos;s engine, is what makes the game go.&#xd;&#xa;&#xd;&#xa;Generally though, the concept of a game engine is fairly simple: it exists to abstract the (sometime platform-dependent) details of doing common game-related tasks, like rendering, physics, and input, so that developers (artists, designers, scripters and, yes, even other programmers) can focus on the details that make their games unique." ID="ID_994893911" CREATED="1389807954371" MODIFIED="1389807954371" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/vergleich%20von%203d%20game%20engines%20-%20unreal3%20vs%20irrlicht.pdf">
<pdf_annotation type="COMMENT" page="7" object_id="8861267699896445322" object_number="134" document_hash="712DC5E1426993FDD9C6FF272E88F2D444FCE6492F125A394271F6BAB8D4AF38">
    <pdf_title>Vergleich von 3D Game Engines</pdf_title>
</pdf_annotation>
</node>
<node TEXT="Wikipedia.de (http://de.wikipedia.org/wiki/Game_Engine)&#xd;&#xa;&#xd;&#xa;Eine Spiel-Engine (englisch game engine [&#x2c8;ge&#x26a;m&#x2cc;&#x25b;nd&#x292;&#x26a;n], deutsch etwa Spiel-Motor) ist ein spezielles Framework f&#xfc;r Computerspiele, die den Spielverlauf steuert und f&#xfc;r die visuelle Darstellung des Spieleablaufes verantwortlich ist. &#xd;&#xa;In der Regel werden derartige Plattformen auch als Entwicklungsumgebung genutzt und bringen daf&#xfc;r auch die n&#xf6;tigen Werkzeuge mit.&#xd;&#xa;&#xd;&#xa;Die Engine besteht, je nach Spiel, unter anderem aus folgenden Bestandteilen:&#xd;&#xa;- Grafik-Engine&#xd;&#xa;- Physiksystem&#xd;&#xa;- Soundsystem&#xd;&#xa;- Datenverwaltung (Laden, Speichern, Speicherverwaltung)&#xd;&#xa;- Steuerung&#xd;&#xa;- Netzwerk-Code&#xd;&#xa;- Skripting" ID="ID_1013134619" CREATED="1389807954278" MODIFIED="1389807954293" LINK="project://143738CC5714RZQ0NZU0OICA1AOZ0EUKMYAG/../literature/Game%20Engines/vergleich%20von%203d%20game%20engines%20-%20unreal3%20vs%20irrlicht.pdf">
<pdf_annotation type="COMMENT" page="7" object_id="6691010766348272683" object_number="136" document_hash="712DC5E1426993FDD9C6FF272E88F2D444FCE6492F125A394271F6BAB8D4AF38">
    <pdf_title>Vergleich von 3D Game Engines</pdf_title>
</pdf_annotation>
</node>
</node>
</node>
</node>
</node>
</map>
